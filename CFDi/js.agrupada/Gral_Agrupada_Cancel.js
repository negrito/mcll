/* ============================================================================================== */
	// Script name	  :  Facturacion Agrupada | Scheduled |Cancel
	// Script type	  :  Schedule
	// Script deploys : 
	// Description 	  : 
	// Author		  :  Ivan Gonzalez
	// Lang 		  : Javascript
	// Date		      : 04-01-2016
	// Dependencias   : Campo customizado con id "custbody_massive_processid".
/* ============================================================================================== */
var CUSTOM_ERROR_CODE = 'Error_proceso_masivo_cancel';

function schedule_cancel_recordid (){

	var MIN_REMAIN_USAGE = 3000;
	var numLine          = nlapiGetContext().getSetting('SCRIPT','custscript_numlinea_cancel');
        numLine          = (numLine) ? parseInt(numLine) : 1; 
    var idToAssign       = nlapiGetContext().getSetting('SCRIPT','custscript_id_registro');
	var recordtype       = (nlapiGetContext().getSetting('SCRIPT','custscript_type_registro')) ? nlapiGetContext().getSetting('SCRIPT','custscript_type_registro') : 'customrecord_process_massivecfdi';
	var obj              = nlapiLoadRecord(recordtype, idToAssign);
	var tipoTrans        = obj.getFieldValue('custrecord_proc_cfdi_masiv_type');
	var xml              = obj.getFieldValue('custrecord_proc_cfdi_masiv_xml');
	switch(tipoTrans){
		case "7": tipoTrans 	= 'invoice'; break;
		case "10": tipoTrans 	= 'creditmemo';break;
	}
	
	var tam_lista = obj.getLineItemCount("recmachcustrecord_dpfm_parent");
	var id_fact_actual;
	var metrica;

	for(var i = numLine ; i <= tam_lista; i++){
		metrica = nlapiGetContext().getRemainingUsage();
		if((metrica < MIN_REMAIN_USAGE) && (i <= tam_lista)){							
			var params = new Array();				
				params['custscript_numlinea_cancel'] = i;
				params['custscript_id_registro']     = idToAssign;
			
			var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);				
			if(status == 'QUEUED'){
				nlapiLogExecution('DEBUG','Reencolado','Se termino la metrica del script: Posición i = ' + i);
				return;
			}
		}
		else{
			try	{
				nlapiLogExecution('DEBUG', 'id_fact_actual: ', id_fact_actual);
				id_fact_actual 	= obj.getLineItemValue("recmachcustrecord_dpfm_parent", "custrecord_dpfm_ids", i);
				nlapiSubmitField(tipoTrans, id_fact_actual, ["custbody_massive_processid", "custbody_cfdixml", "custbody_cancelcfdi", "custbody_foliosat", 'custbody_cfdi_pdf', 'custbody_uuid'], ['','','','','','']);
			}
			catch (ex){
				nlapiLogExecution('ERROR', ex instanceof nlobjError ? ex.getCode() : CUSTOM_ERROR_CODE,
				ex instanceof nlobjError ? ex.getDetails() : 'JavaScript Error: ' + (ex.message != null ? ex.message : ex));
			}
		}
	}

	nlapiSubmitField(obj.getRecordType(), obj.getId(), "custrecord_proc_cfdi_masiv_nocanceled", 'T');
}