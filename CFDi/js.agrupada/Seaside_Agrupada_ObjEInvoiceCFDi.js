//===========================================================================================
// Company        : NetSoft, www.netsoft.com.mx
// Name of Script : Gral_Agrupada_ObjEInvoiceCFDi.js
// Author         : Ivan Gonzalez - Netsoft - Mexico
// Date			  : 09-09-201
// Base			  : DEV
// Type           : Library
// Sub-Type       : Business Process
// Categories     : Business logic
// Description    : Generate eInvoice layout 
// NetSuite Ver.  : (Edition: International) Release 2011.2
// Dependences    : File Name
//                   --------------------
//                   NSOUtils.js
//                   NSOCountriesStates.js
//===========================================================================================

var taxexentogroup = -8;


function NSObjEInvoiceCFDiAgrupado() 
{    	
    this.invoice       = null;
    this.customer      = null;
    this.params        = null;
	this.setupcfdi     = null;
    this.salesorder    = null;
	this.witemid       = null;
	this.body          = "";	
	this.subtotal      = 0;
	this.subtotalbruto = 0;
	
	nlapiLogExecution('DEBUG', 'entro al xml: ', '');
    this.build = function(wprocessid, setupid, wtype)
    {
		setupid  = setupid == null || setupid == "" ? 1 : setupid;
	
	    //----> Obtiene datos generales
        var invoice     = nlapiLoadRecord('customrecord_process_massivecfdi', wprocessid); 
		var custid      = invoice.getFieldValue("custrecord_proc_cfdi_masiv_customer");
		var customer    = nlapiLoadRecord("customer", custid);
        var params      = nlapiLoadRecord("customrecord_cfdisetup", setupid);

		var setupcfdi   = nlapiLoadRecord("customrecord_setup_cfdi", 1);
		var version     = setupcfdi.getFieldValue('custrecord_cfdi_version')||'5';
		//var id = wprocessid;
        //----> Asignación de variables globales
        //this.id          = id;
        this.invoice     = invoice;
        this.customer    = customer;	
        this.params      = params;        
		this.setupcfdi   = setupcfdi;
		this.wtype       = wtype;
		
		
		//----> Generación de XML
		var bodytext = '';
		bodytext += '<fx:FactDocMX xmlns:fx="http://www.fact.com.mx/schema/fx" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.fact.com.mx/schema/fx http://www.mysuitemex.com/fact/schema/fx_2010_d.xsd">';

        bodytext += '<fx:Version>5</fx:Version>';
      	bodytext += nlGetCFDi01(invoice, customer, params, setupcfdi, wtype);
		nlapiLogExecution('DEBUG', 'nlGetCFDi01: ', bodytext);
		bodytext += nlGetCFDiH4(invoice, customer, params, setupcfdi);
		//nlapiLogExecution('DEBUG', 'nlGetCFDiH4: ', bodytext);
		bodytext += nlGetCFDi02(invoice, customer, params, setupcfdi);
		//nlapiLogExecution('DEBUG', 'nlGetCFDi02: ', bodytext);
		bodytext += Totales(invoice, customer, params, setupcfdi);
		//nlapiLogExecution('DEBUG', 'Totales: ', bodytext);
		bodytext += ComprobanteEx(invoice, customer, params, setupcfdi);
		//nlapiLogExecution('DEBUG', 'ComprobanteEx: ', bodytext);
		
		bodytext = bodytext.replace(/á/g, "&#225;");
		bodytext = bodytext.replace(/é/g, "&#233;");
		bodytext = bodytext.replace(/í/g, "&#237;");
		bodytext = bodytext.replace(/ó/g, "&#243;");
		bodytext = bodytext.replace(/ú/g, "&#250;");
		bodytext = bodytext.replace(/Á/g, "&#193;");
		bodytext = bodytext.replace(/É/g, "&#201;");
		bodytext = bodytext.replace(/Í/g, "&#205;");
		bodytext = bodytext.replace(/Ó/g, "&#211;");
		bodytext = bodytext.replace(/Ú/g, "&#218;");
		bodytext = bodytext.replace(/Ò/g, "&#210;");
		bodytext = bodytext.replace(/ñ/g, "&#241;");
		bodytext = bodytext.replace(/Ñ/g, "&#209;");
		bodytext = bodytext.replace(/®/g, "&#174;");
		bodytext = bodytext.replace(/©/g, "&#169;");
		bodytext = bodytext.replace(/ª/g, "&#170;");
		bodytext = bodytext.replace(/™/g, "&#153;");
		bodytext = bodytext.replace(/Ÿ/g, "&#159;");
		bodytext = bodytext.replace(/°/g, "&#176;");
		bodytext = bodytext.replace(/¼/g, "&#188;");
		bodytext = bodytext.replace(/½/g, "&#189;");
		bodytext = bodytext.replace(/¾/g, "&#190;");
		bodytext = bodytext.replace(/Ä/g, "&#196;");
		bodytext = bodytext.replace(/Ë/g, "&#203;");
		bodytext = bodytext.replace(/Ï/g, "&#207;");
		bodytext = bodytext.replace(/Ö/g, "&#214;");
		bodytext = bodytext.replace(/Ü/g, "&#220;");
		bodytext = bodytext.replace(/ÿ/g, "&#255;");
		this.body = bodytext;
		//nlapiLogExecution('DEBUG', 'bodytext: ', bodytext);
        return bodytext;
    }
}




//-----------------------------------Inicia trama 01------------------------------------------------------------------------------------
 

function nlGetCFDi01(invoice, customer, params, setupcfdi, wtype) 
{
	var wtype = invoice.getFieldValue('custrecord_proc_cfdi_masiv_type');
	nlapiLogExecution('DEBUG', 'wtype: ', wtype);
    var folio                = invoice.getId();
	var FolioFiscalOrigUuid  = '';
	var SerieFolioFiscalOrig = '';
	var FechaFolioFiscalOrig = '';
	var MontoFolioFiscalOrig = '';
	var emailsend            = params.getFieldValue('custrecord_cfdi_emailreceipt');
	var emailcustomer        = isNull(customer.getFieldValue('email'));
	var sendemail            = setupcfdi.getFieldValue('custrecord_cfdi_mailalcliente');
	var emailtest            = setupcfdi.getFieldValue('custrecord_cfdi_email_send_test');
	//var testing              = setupcfdi.getFieldValue('custrecord_cfdi_testing');
	var testingmasiva        = setupcfdi.getFieldValue('custrecord_cfdi_testing_masiva');
	var TipoDeComprobante    = isNull(getTipoDocumento(wtype));
	//nlapiLogExecution("DEBUG", "TipoDeComprobante:  ", TipoDeComprobante);
	//----> Datos Emisor
	var xrwtype  = '';
	if (wtype == '7' ){var xrwtype = 'FACTURA'}
	else{xrwtype = 'NOTA_DE_CREDITO'}
	
	var retval = '';	
	retval += '<fx:Identificacion>';
	retval += '<fx:CdgPaisEmisor>MX</fx:CdgPaisEmisor>';	
	retval += '<fx:TipoDeComprobante>' + xrwtype+ '</fx:TipoDeComprobante>';	
	//retval += '<fx:TipoDeComprobante>FACTURA</fx:TipoDeComprobante>';	
	
	if(testingmasiva == 'T')	
	{
	 	retval += '<fx:RFCEmisor>' + setupcfdi.getFieldValue('custrecord_cfdi_entity_testing') + '</fx:RFCEmisor>';	
	}
	else
	{
		retval += '<fx:RFCEmisor>' + params.getFieldValue('custrecord_cfdi_rfcemisor') + '</fx:RFCEmisor>';	
	}
	
	var razonsocial = isNull(params.getFieldValue('custrecord_cfdi_nombreemisor'))
	
	if (razonsocial != null && razonsocial != ''){								  		  
	retval += '<fx:RazonSocialEmisor>' + razonsocial + '</fx:RazonSocialEmisor>';	 
	}
	
	if(testingmasiva == 'T')	
	{
	 	retval += '<fx:Usuario>' + setupcfdi.getFieldValue('custrecord_cfdi_user_testing') + '</fx:Usuario>';	
	}
	else
	{
		retval += '<fx:Usuario>' + params.getFieldValue('custrecord_cfdi_usuario') + '</fx:Usuario>';	
	}
	
	retval += '<fx:NumeroInterno>'  +  folio + '</fx:NumeroInterno>';
	
	var municipioexpedicion = isNull(params.getFieldValue('custrecord_cfdi_mun_expedidoen'));
	var estadoexpedicion    = isNull(params.getFieldValue('custrecord_cfdi_estadoemisor'));
	
	if (( municipioexpedicion != null && municipioexpedicion != '')||(estadoexpedicion != null && estadoexpedicion != ''))
	{
		if (( municipioexpedicion != null && municipioexpedicion != '')&&(estadoexpedicion != null && estadoexpedicion != ''))
		{retval += '<fx:LugarExpedicion>'  + municipioexpedicion + ', ' + estadoexpedicion + '</fx:LugarExpedicion>';	}
		else
		{
			if (( municipioexpedicion != null && municipioexpedicion != '')&& (estadoexpedicion == null || estadoexpedicion == ''))
			{
				retval += '<fx:LugarExpedicion>'  + municipioexpedicion + '</fx:LugarExpedicion>';
			}
			if (( municipioexpedicion == null || municipioexpedicion == '')&& (estadoexpedicion != null && estadoexpedicion != ''))
			{
				retval += '<fx:LugarExpedicion>'  + estadoexpedicion + '</fx:LugarExpedicion>';
			}
		}
	}
	
	var ctapago =  isNull(invoice.getFieldValue('custbody_cfdi_numctapago'));
	if (ctapago == null || ctapago == '')
	{
		var idctapago =  isNull(params.getFieldValue('custrecord_cfdi_cuenta_pago'));
		ctapago =  isNull(customer.getFieldValue(idctapago));
	}	
	
	if 	(ctapago != null && ctapago != '')
		{retval += '<fx:NumCtaPago>' + ctapago + '</fx:NumCtaPago>';	}
	
	retval += '</fx:Identificacion>';		
	
	
	//------> Envio de correo a cuenta de pruebas
	if (testingmasiva == 'T' )
	{
		//nlapiLogExecution("DEBUG", "emailtest:  ", emailtest);
		if(emailtest != null && emailtest != '') {
			retval += '<fx:Procesamiento>';
			retval += '<fx:Dictionary name="email">';
			retval += '<fx:Entry k="to" v="' +emailtest+'"/>';
			retval += '</fx:Dictionary>';
			retval += '</fx:Procesamiento>';
		}
	}
	else {
		
		//------> Envio de correo al cliente
		if (sendemail == 'T')
		{	
			if ((emailsend != null && emailsend != '')||(emailcustomer != null && emailcustomer != ''))
			{
				retval += '<fx:Procesamiento>';
				retval += '<fx:Dictionary name="email">';
				if (emailcustomer != null && emailcustomer != '' && emailsend != null && emailsend != "")
				{
					retval += '<fx:Entry k="to" v="' + emailcustomer + ';'+ emailsend +'"/>';
				}
				else if((emailcustomer == null || emailcustomer == '') && emailsend != null && emailsend != "")
				{
					retval += '<fx:Entry k="to" v="' +emailsend+'"/>';
				}
				else if((emailsend == null || emailsend == '') && emailcustomer != null && emailcustomer != "")
				{
					retval += '<fx:Entry k="to" v="' + emailcustomer +'"/>';
				}
				retval += '</fx:Dictionary>';
				retval += '</fx:Procesamiento>';
			}
		}
		else
		{
			retval += '<fx:Procesamiento>';
			retval += '<fx:Dictionary name="email">';
			if (emailsend != null && emailsend != "")
			{
				retval += '<fx:Entry k="to" v="' + emailsend +'"/>';
			}		
			retval += '</fx:Dictionary>';
			retval += '</fx:Procesamiento>';
		}
	}
	
	//----> Datos Emisor	
	retval     += '<fx:Emisor>';	
	retval     += '<fx:DomicilioFiscal>';	
	var calle   = (params.getFieldValue('custrecord_cfdi_calleemisor'));	
	retval     += '<fx:Calle>' + calle + '</fx:Calle>'; 	   
	var numext = isNull(params.getFieldValue('custrecord_cfdi_numexterioremisor'));
	if (numext != null && numext != '')
	{
		retval += '<fx:NumeroExterior>' + numext + '</fx:NumeroExterior>'; 	    
	}	
	var numint = isNull(params.getFieldValue('custrecord_cfdi_numinterioremisor'));
	if (numint != null && numint != '')
	{																								
   	    retval += '<fx:NumeroInterior>' + numint + '</fx:NumeroInterior>'; 
	}	
	var localidad = isNull(params.getFieldValue('custrecord_cfdi_localidademisor'));
	if (localidad != null && localidad != '')
	{
    	retval += '<fx:Localidad>' + localidad + '</fx:Localidad>';    
	}	
	var referencia = isNull(params.getFieldValue('custrecord_cfdi_referenciaemisor'));
	if (referencia != null && referencia != '')
    {
		retval += '<fx:Referencia>' + referencia + '</fx:Referencia>';   
	}	
	var colonia = isNull(params.getFieldValue('custrecord_cfdi_coloniaemisor'));
	if (colonia != null && colonia != '')
	{
    	retval += '<fx:Colonia>' + colonia + '</fx:Colonia>'; 
	}	
	var municipio = isNull(params.getFieldValue('custrecord_cfdi_municipioemisor'));
	if (municipio != null && municipio != '')
	{
    	retval += '<fx:Municipio>'  + municipio + '</fx:Municipio>';   
	}
	
    retval += '<fx:Estado>' + isNull(params.getFieldValue('custrecord_cfdi_estadoemisor')) + '</fx:Estado>';	    
    retval += '<fx:Pais>'  + isNull(params.getFieldValue('custrecord_cfdi_paisemisor')) + '</fx:Pais>'; 	    
    retval += '<fx:CodigoPostal>'  + isNull(params.getFieldValue('custrecord_cfdi_cpemisor')) + '</fx:CodigoPostal>';  				
	retval += '</fx:DomicilioFiscal>';	
	
	//------------->Empiezan datos del lugar de emision <--------------------------------// 
	
	var estadoemision = isNull(params.getFieldValue('custrecord_cfdi_estado_expedidoen'));
	var paisemision = isNull(params.getFieldValue('custrecord_cfdi_pais_expedidoen'));
	var CPEmision = isNull(params.getFieldValue('custrecord_cfdi_cp_expedidoen'));
	
	if( (estadoemision != null && estadoemision != '') && (paisemision != null && paisemision != '') && (CPEmision != null && CPEmision != '') )
	{
		retval += '<fx:DomicilioDeEmision>';
			
		var calleemi = isNull(params.getFieldValue('custrecord_cfdi_calleexpedidoen'));
		if (calleemi != null && calleemi != '')
		{
			retval += '<fx:Calle>' + calleemi + '</fx:Calle>'; 	   
		}	
		var numextemi = isNull(params.getFieldValue('custrecord_cfdi_numext_expedidoen'));
		if (numextemi != null && numextemi != '')
		{
			retval += '<fx:NumeroExterior>' + numextemi + '</fx:NumeroExterior>'; 	    
		}	
		var numintemi = isNull(params.getFieldValue('custrecord_cfdi_numint_expedidoen'));
		if (numintemi != null && numintemi != '')
		{																								
			retval += '<fx:NumeroInterior>' + numintemi + '</fx:NumeroInterior>'; 
		}	
		var localidademi = isNull(params.getFieldValue('custrecord_cdfi_loc_expedidoen'));
		if (localidademi != null && localidademi != '')
		{
			retval += '<fx:Localidad>' + localidademi + '</fx:Localidad>';    
		}	
		var referenciaemi = isNull(params.getFieldValue('custrecord_cfdi_ref_expedidoen'));
		if (referenciaemi != null && referenciaemi != '')
		{
			retval += '<fx:Referencia>' + referenciaemi + '</fx:Referencia>';   
		}	
		var coloniaemi = isNull(params.getFieldValue('custrecord_cfdi_colonia_expedidoen'));
		if (coloniaemi != null && coloniaemi != '')
		{
			retval += '<fx:Colonia>' + coloniaemi + '</fx:Colonia>'; 
		}
		var municipioemi = isNull(params.getFieldValue('custrecord_cfdi_mun_expedidoen'));
		if (municipioemi != null && municipioemi != '')
		{
			retval += '<fx:Municipio>'  + municipioemi + '</fx:Municipio>';   
		}
		
		retval += '<fx:Estado>' + estadoemision + '</fx:Estado>';	    
		retval += '<fx:Pais>'  + paisemision + '</fx:Pais>'; 	    
		retval += '<fx:CodigoPostal>' + CPEmision + '</fx:CodigoPostal>';  		
		retval += '</fx:DomicilioDeEmision>';
	}
		
	retval += '<fx:RegimenFiscal>';
	retval += '<fx:Regimen>' + isNull(params.getFieldValue('custrecord_cfdi_regimenfiscal')) + '</fx:Regimen>'
	retval += '</fx:RegimenFiscal>';	    	
	retval += '</fx:Emisor>';	
	retval  = retval.replace(/&/g, "&amp;");
	
	
    return retval;
}




function nlGetCFDiH4(invoice, customer, params, setupcfdi)
{
	//----> Datos del receptor
  	var retval      = '';
	var IdRFCcustomer = setupcfdi.getFieldValue('custrecord_cfdi_entity_rfc');
	nlapiLogExecution('DEBUG', 'IdRFCcustomer: ', IdRFCcustomer);
	var recRFC      = isNull(customer.getFieldValue(IdRFCcustomer));
	nlapiLogExecution('DEBUG', 'recRFC: ', recRFC);
	var nombreenvio = isNull(customer.getFieldValue('companyname'));
	//nlapiLogExecution('DEBUG', 'nombreenvio: ', nombreenvio);
  	for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++) 
	{
		var defaultbilling = customer.getLineItemValue('addressbook', 'defaultbilling', i);
		//nlapiLogExecution('DEBUG', 'defaultbilling: ', defaultbilling);
		if (defaultbilling == 'T')
		{
			var codigopais    = (customer.getLineItemValue('addressbook', 'country', i));
			var pais          = nsoGetCountryName(customer.getLineItemValue('addressbook', 'country', i));
			var callereceptor = isNull(customer.getLineItemValue('addressbook', 'addr1', i));
			var colonia       =  isNull(customer.getLineItemValue('addressbook', 'addr2', i));
			var Municipio     = isNull(customer.getLineItemValue('addressbook', 'city', i));
			var Estado        = isNull(customer.getLineItemValue('addressbook', 'state', i));
			var CP            = isNull(customer.getLineItemValue('addressbook', 'zip', i));								
			
			if (codigopais != 'MX'){recRFC = 'XEXX010101000'}
			
			retval = '<fx:Receptor>';	
			retval += '<fx:CdgPaisReceptor>' + codigopais + '</fx:CdgPaisReceptor>';
			retval += '<fx:RFCReceptor>' + recRFC + '</fx:RFCReceptor>'; 
			if (nombreenvio != null && nombreenvio != '')
			{retval += '<fx:NombreReceptor>' + nombreenvio + '</fx:NombreReceptor>';}
			retval += '<fx:Domicilio>';
			
			if (codigopais != 'MX')	
			{retval += '<fx:OtroDomicilio>';}
			else
			{retval += '<fx:DomicilioFiscalMexicano>';}
			if (callereceptor != null && callereceptor != '')
			{retval += '<fx:Calle>' + callereceptor + '</fx:Calle>';}   
			if (colonia != null && colonia != '')
			{retval += '<fx:Colonia>' + colonia + '</fx:Colonia>';}
			if (Municipio != null && Municipio !='')
			{retval += '<fx:Municipio>' + Municipio + '</fx:Municipio>';}
			if (Estado != null && Estado != '')
			{retval += '<fx:Estado>' + Estado + '</fx:Estado>';}
			if (pais != null && pais != '')
			retval += '<fx:Pais>' + pais + '</fx:Pais>';           
			if (CP != null && CP != '')
			{retval += '<fx:CodigoPostal>' + CP + '</fx:CodigoPostal>';} 
				
			if (codigopais != 'MX')	
			{retval += '</fx:OtroDomicilio>';}
			else
			{retval += '</fx:DomicilioFiscalMexicano>';}
			
			retval += '</fx:Domicilio>';
			retval += '</fx:Receptor>';
		}
	}
		
	retval = retval.replace(/&/g, "&amp;");
	retval = retval.replace(/"/g, "&quot;");
	
    return retval;
}




//=============================inicia trama 02=================================================================


function nlGetCFDi02(invoice, customer, params, setupcfdi)
{
	var subtotal        = 0;
	var subtotalbruto   = 0; 
	var taxexento       = isNull(setupcfdi.getFieldValue('custrecord_cfdi_taxexento'));
	var taxcodecero		= isNull(setupcfdi.getFieldValue('custrecord_cfdi_taxcero'));
	var taxcode11 		= isNull(setupcfdi.getFieldValue('custrecord_cfdi_tax11'));
	var taxcode16		= isNull(setupcfdi.getFieldValue('custrecord_cfdi_tax16'));
	var taxexentogroup = -8;
	var wtaxrate  = '';
	var tax       = '';
	   
    var retval    = '<fx:Conceptos>';
	for(var  h = 1; h <= invoice.getLineItemCount("recmachcustrecord_dfm_fieldparent"); h++)
	{
		var wamount   = nsoParseFloatOrZero(invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent", 'custrecord_dfm_amount', h));
		var wtaxamt   = nsoParseFloatOrZero(invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent", 'custrecord_dfm_tax', h));
		var wtaxcode  = invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent" , 'custrecord_dfm_taxcodeid', h);
		//witem     = invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent" , 'custrecord_dfm_item', h);
		var wdescitem = invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent" , 'custrecord_dfm_item_display', h);
		var wquantity = nsoParseFloatOrZero(invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent" , 'custrecord_dfm_quantity', h));
		var wrate     = nsoParseFloatOrZero(invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent" , 'custrecord_dfm_rate', h));
		
		if (wtaxcode  == taxcode16)
		{wtaxrate = 16.00;}
		if (wtaxcode  == taxcode11)
		{wtaxrate = 11.00;}
		if (wtaxcode  == taxcodecero)
		{wtaxrate = 0.00;}
		if (wtaxcode  == taxexento)
		{wtaxrate = 0.00;}
		nlapiLogExecution('DEBUG', 'wtaxrate: ', wtaxrate);
		if (wtaxrate >= 0)
		{			
			retval += '<fx:Concepto>';				 
			retval += '<fx:Cantidad>' + isNull(wquantity) + '</fx:Cantidad>'; 
			retval += '<fx:UnidadDeMedida>No Aplica</fx:UnidadDeMedida>';
			retval += '<fx:Descripcion>' + wdescitem +  '</fx:Descripcion>'; 
			retval += '<fx:ValorUnitario>' + wrate.toFixed(4) + '</fx:ValorUnitario>';
			retval += '<fx:Importe>' + (wrate*wquantity).toFixed(4) + '</fx:Importe>';
			subtotal += parseFloat(wrate*wquantity);
			retval += '<fx:ConceptoEx>';
						
			retval         += '<fx:PrecioLista>' + wrate.toFixed(4) + '</fx:PrecioLista>'; 														
			retval         += '<fx:ImporteLista>' + (wrate*wquantity).toFixed(4) + '</fx:ImporteLista>';
			subtotalbruto   = parseFloat(subtotalbruto) + parseFloat(wrate*wquantity);
			/*nlapiLogExecution('DEBUG', 'wtaxcode: ', wtaxcode);
			nlapiLogExecution('DEBUG', 'taxexento: ', taxexento);
			nlapiLogExecution('DEBUG', 'taxexentogroup: ', taxexentogroup);*/
			if (wtaxcode != taxexento && wtaxcode != taxexentogroup)
			{
				retval += '<fx:Impuestos>';
				retval += '<fx:Impuesto>';
				retval += '<fx:Contexto>' + 'FEDERAL'+ '</fx:Contexto>';	
				retval += '<fx:Operacion>' + 'TRASLADO'+ '</fx:Operacion>';	
				retval += '<fx:Codigo>' + 'IVA'+ '</fx:Codigo>';	
				retval += '<fx:Base>' + nsoParseFloatOrZero(wamount).toFixed(2) + '</fx:Base>';	
				retval += '<fx:Tasa>' + nsoParseFloatOrZero(wtaxrate).toFixed(2) + '</fx:Tasa>';
				var monto4 = nsoParseFloatOrZero((wamount)*(wtaxrate/100)).toFixed(2);
				retval += '<fx:Monto>' + monto4 + '</fx:Monto>';
				retval += '</fx:Impuesto>';
				retval += '</fx:Impuestos>'	
			}
		
			retval += '</fx:ConceptoEx>';
			retval += '</fx:Concepto>';
		}
	
	}//end for
		
	this.subtotal      = subtotal;
	this.subtotalbruto = subtotalbruto;

	retval = retval.replace(/&/g, "&amp;");
	retval = retval.replace(/"/g, "&quot;");
    retval += '</fx:Conceptos>';
	
    return retval;
	
}


//=================================finaliza trama 02 ==============================================

function Totales(invoice, customer, params, setupcfdi) 
{
	
	var currency 	 = invoice.getFieldValue('custrecord_proc_cfdi_masiv_currency');
	nlapiLogExecution("DEBUG", "currency:  ", currency);
	if (currency == null || currency == '')
	{
		var moneda       = 'MXN';
		var tipodecambio = '1.0';
		var symbol 		 = 'MXN';
	}
	else
	{
		var moneda 		 = nlapiLoadRecord('currency', currency);
		var symbol 		 = moneda.getFieldValue('symbol');
		if (symbol == 'MXN'){var tipodecambio = 1;}
		else{
		var tipodecambio = moneda.getFieldValue('exchangerate');}
	}
    var taxexento    = isNull(setupcfdi.getFieldValue('custrecord_cfdi_taxexento'));
	var taxcodecero	 = isNull(setupcfdi.getFieldValue('custrecord_cfdi_taxcero'));
	var taxcode11 	 = isNull(setupcfdi.getFieldValue('custrecord_cfdi_tax11'));
	var taxcode16	 = isNull(setupcfdi.getFieldValue('custrecord_cfdi_tax16'));
	var taxexentogroup = -8;	
	var retval = '<fx:Totales>';	                                                   
	retval += '<fx:Moneda>' + isNull(symbol) + '</fx:Moneda>'; 												   
	retval += '<fx:TipoDeCambioVenta>' + tipodecambio + '</fx:TipoDeCambioVenta>';	
	retval += '<fx:SubTotalBruto>' + parseFloat(this.subtotalbruto).toFixed(2) + '</fx:SubTotalBruto>';
	retval += '<fx:SubTotal>' + parseFloat(this.subtotal).toFixed(2) + '</fx:SubTotal>';
	
	retval += '<fx:ResumenDeDescuentosYRecargos>';	
	retval += '<fx:TotalDescuentos>0.00</fx:TotalDescuentos>';			
	retval += '<fx:TotalRecargos>0.00</fx:TotalRecargos>';
	retval += '</fx:ResumenDeDescuentosYRecargos>';	
	
	var tax       = '';
	var wtaxrate  = '';
	var wtaxtotal = 0;
	var total     = 0;
	var retvalimp = '';
	
	for(var i = 1; invoice != null && i <= invoice.getLineItemCount('recmachcustrecord_dfm_fieldparent'); i++)
	{	
		var wamount   = nsoParseFloatOrZero(invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent", 'custrecord_dfm_amount', i));
		var wtaxamt   = nsoParseFloatOrZero(invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent", 'custrecord_dfm_tax', i));
		var wtaxcode  = invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent" , 'custrecord_dfm_taxcodeid', i);
		
		if (wtaxcode  == taxcode16)
		{wtaxrate = 16.00;}
		if (wtaxcode  == taxcode11)
		{wtaxrate = 11.00;}
		if (wtaxcode  == taxcodecero)
		{wtaxrate = 0.00;}
		if (wtaxcode  == taxexento)
		{wtaxrate = 0.00;}
		nlapiLogExecution('DEBUG', 'wtaxcode: ', wtaxcode);
		nlapiLogExecution('DEBUG', 'taxexento: ', taxexento);
			
		if ((wtaxrate >=0)&&(wtaxcode != taxexento && wtaxcode != taxexentogroup))
		{
			retvalimp += '<fx:Impuesto>';
			retvalimp += '<fx:Contexto>' + 'FEDERAL'+ '</fx:Contexto>';	
			retvalimp += '<fx:Operacion>' + 'TRASLADO'+ '</fx:Operacion>';	
			retvalimp += '<fx:Codigo>' + 'IVA'+ '</fx:Codigo>';	
			retvalimp += '<fx:Base>' + nsoParseFloatOrZero(wamount).toFixed(2) + '</fx:Base>';	
			retvalimp += '<fx:Tasa>' + nsoParseFloatOrZero(wtaxrate).toFixed(2) + '</fx:Tasa>';
			retvalimp += '<fx:Monto>' + nsoParseFloatOrZero(wtaxamt).toFixed(2) + '</fx:Monto>';
			retvalimp += '</fx:Impuesto>';
			
			wtaxtotal += nsoParseFloatOrZero(wtaxamt);
			
		}
		total += nsoParseFloatOrZero(wamount);
	}
	
	if (retvalimp != null && retvalimp != '')
	{
		retval += '<fx:Impuestos>';
		retval += retvalimp;
		retval += '</fx:Impuestos>'	
		
	}
	
	retval += '<fx:ResumenDeImpuestos>';		
	retval += '<fx:TotalTrasladosFederales>' + nsoParseFloatOrZero(wtaxtotal).toFixed(4) + '</fx:TotalTrasladosFederales>';
	retval += '<fx:TotalIVATrasladado>' + nsoParseFloatOrZero(wtaxtotal).toFixed(4) + '</fx:TotalIVATrasladado>';
	retval += '<fx:TotalIEPSTrasladado>' + '0' + '</fx:TotalIEPSTrasladado>';
	retval += '<fx:TotalRetencionesFederales>' + '0' + '</fx:TotalRetencionesFederales>';
	retval += '<fx:TotalISRRetenido>' + '0' + '</fx:TotalISRRetenido>';
	retval += '<fx:TotalIVARetenido>' + '0' + '</fx:TotalIVARetenido>';
	retval += '<fx:TotalTrasladosLocales>' + '0' + '</fx:TotalTrasladosLocales>';
	retval += '<fx:TotalRetencionesLocales>' + '0' + '</fx:TotalRetencionesLocales>';	
	retval += '</fx:ResumenDeImpuestos>';	
	
	var newtotal = nsoParseFloatOrZero(total) + nsoParseFloatOrZero(wtaxtotal);
	newtotal = nsoParseFloatOrZero(newtotal).toFixed(2);
	var objConverter = new NSObjConverterAmountToWords();
	var words = objConverter.toWords(newtotal, "spanish", symbol);
	//nlapiLogExecution('DEBUG','words', words);
	retval += '<fx:Total>' + nsoParseFloatOrZero(newtotal).toFixed(4) + '</fx:Total>';
	retval += '<fx:TotalEnLetra>' + words + '</fx:TotalEnLetra>';
	
	var FormaDePago = isNull(invoice.getFieldText('custbody_cfdi_formadepago'));
	
	if (FormaDePago == null || FormaDePago == '')
	{
		idFormaDePago = isNull(params.getFieldText('custrecord_cfdi_formapago_customer'));
		FormaDePago = isNull(customer.getFieldText(idFormaDePago));}
	
	if (FormaDePago != null && FormaDePago != '')	
	{retval += '<fx:FormaDePago>' + FormaDePago + '</fx:FormaDePago>'; }	
	else
	{retval += '<fx:FormaDePago>PAGO EN UNA SOLA EXHIBICION</fx:FormaDePago>'; }	
	retval += '</fx:Totales>';

	return retval;
	
}





//================================== Comprobante Ex =======================================


function ComprobanteEx (invoice, customer, params, setupcfdi)
{
	var retval = '';
	var testing        = setupcfdi.getFieldValue('custrecord_cfdi_testing');
	var testingmasiva  = setupcfdi.getFieldValue('custrecord_cfdi_testing_masiva');
	var metododepago   = isNull(invoice.getFieldValue('custrecord_proc_cfdi_masiv_metpago'));
	
	retval += '<fx:ComprobanteEx>';
	retval += '<fx:DatosDeNegocio>';
	
	if(testingmasiva == 'T'){
		retval += '<fx:Sucursal>' + setupcfdi.getFieldValue('custrecord_cfdi_sucursal_testing') + '</fx:Sucursal>';
	}
	else{
		retval += '<fx:Sucursal>' + params.getFieldValue('custrecord_cfdi_sucursal_mysuite') + '</fx:Sucursal>';
	}
	retval += '</fx:DatosDeNegocio>';
	
	var claveMetodoPago = '99';
	nlapiLogExecution('DEBUG','metododepago: ' + metododepago);
	if (metododepago != null && metododepago != '')	{
		claveMetodoPago = MetPago(metododepago);
	}
	retval += '<fx:TerminosDePago>';
	retval += '<fx:MetodoDePago>' + claveMetodoPago + '</fx:MetodoDePago>';
	retval += '</fx:TerminosDePago>';
	
	
	retval += '</fx:ComprobanteEx>';
	retval += '</fx:FactDocMX>'; 
	retval = retval.replace(/&/g, "&amp;");
	retval = retval.replace(/"/g, "&quot;");
	return retval;
}
  		
		

//=============================== Fin Comprobante Ex ========================================

function MetPago(idMetdPago)
{
	var retval = '';
	if(idMetdPago){
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_cfdi_payment_met_nat', null, 'anyof', idMetdPago));
		   
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_cfdi_payment_met_sat'));

		var searchresult  = nlapiSearchRecord('customrecord_cfdi_metododepago', null, filters, columns);
		nlapiLogExecution('DEBUG','searchresult: ' + searchresult);
		for(var i = 0 ; searchresult != null && i < searchresult.length; i++){
			var idMet          = searchresult[i].getText('custrecord_cfdi_payment_met_sat');
			if(idMet != null && idMet != ''){
				var arrayMP = new Array();
				arrayMP = idMet.split('-');
				if(arrayMP.length > 1){
					retval = arrayMP[0];
				}
			}
		}
	}

	return retval;

}


function AgrupaIVA(record, taxexento, taxexentogroup)
{
	var arrTax = new Array();
	
	for(var i = 1; record != null && i <= record.getLineItemCount('recmachcustrecord_dfm_fieldparent'); i++)
	{
		var amount   = nsoParseFloatOrZero(invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent", 'custrecord_dfm_amount', h));
		var taxamt   = nsoParseFloatOrZero(invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent", 'custrecord_dfm_tax', h));
		var taxcode  = invoice.getLineItemValue("recmachcustrecord_dfm_fieldparent" , 'custrecord_dfm_taxcodeid', h);
		var tax       = parseFloat(record.getLineItemValue('item', 'taxrate1', i)); 	
									
		if (taxcode != null && taxcode != ""  && taxcode != taxexento && taxcode != taxexentogroup)
		{						
			var index = null;
				
			try
			{
				index = arrTax[taxcode].length;
			}
			catch(err)
			{			
			}
		
			if (index == null)
			{
				arrTax[taxcode] = new Array();
			}
			
			index = arrTax[taxcode].length;								
			arrTax[taxcode][index] = new Tax(tax, taxcode, amount);
		}		
	}
	
	return arrTax;
}


function Tax(tax, taxcode, amount)
{
	this.tax = tax;
	this.taxcode = taxcode;
	this.amount = amount;	
}

function CdgPaisReceptor(pais)
{	
	var retval = '';
	
	
		if (pais == 'United States' || pais == 'Estados Unidos' || pais == 'UNITED STATES')	{}
		if(naduana == '02')	{retval = 'AGUA PRIETA';}
		if(naduana == '05')	{retval = 'SUBTENIENTE LOPEZ';}
		if(naduana == '06')	{retval = 'CIUDAD DEL CARMEN';}
		if(naduana == '07')	{retval = 'CIUDAD JUAREZ, CIUDAD JUAREZ, CHIHUAHUA.';}
		if(naduana == '08')	{retval = 'COATZACOALCOS, COATZACOALCOS, VERACRUZ.';}
		if(naduana == '11')	{retval = 'ENSENADA';}
		if(naduana == '12')	{retval = 'GUAYMAS';}
		if(naduana == '14')	{retval = 'LA PAZ';}
		if(naduana == '16')	{retval = 'MANZANILLO';}
		if(naduana == '17')	{retval = 'MATAMOROS';}
		if(naduana == '18')	{retval = 'MAZATLAN';}
		if(naduana == '19')	{retval = 'MEXICALI';}
		if(naduana == '20')	{retval = 'MEXICO';}
		if(naduana == '22')	{retval = 'NACO';}
		if(naduana == '23')	{retval = 'NOGALES';}
		if(naduana == '24')	{retval = 'NUEVO LAREDO';}
		if(naduana == '25')	{retval = 'OJINAGA';}
		if(naduana == '26')	{retval = 'PUERTO PALOMAS';}
		if(naduana == '27')	{retval = 'PIEDRAS NEGRAS';}
		if(naduana == '28')	{retval = 'PROGRESO';}
		if(naduana == '30')	{retval = 'CIUDAD REYNOSA';}
		if(naduana == '31')	{retval = 'SALINA CRUZ';}
		if(naduana == '33')	{retval = 'SAN LUIS RIO COLORADO';}
		if(naduana == '34')	{retval = 'CIUDAD MIGUEL ALEMAN';}
		if(naduana == '37')	{retval = 'CIUDAD HIDALGO';}
		if(naduana == '38')	{retval = 'TAMPICO';}
		if(naduana == '39')	{retval = 'TECATE';}
		if(naduana == '40')	{retval = 'TIJUANA';}
		if(naduana == '42')	{retval = 'TUXPAN';}
		if(naduana == '43')	{retval = 'VERACRUZ';}
		if(naduana == '44')	{retval = 'CIUDAD ACUÑA';}
		if(naduana == '46')	{retval = 'TORREON';}
		if(naduana == '47')	{retval = 'APTO INTL CD MEXICO';}
		if(naduana == '48')	{retval = 'GUADALAJARA';}
		if(naduana == '50')	{retval = 'SONOYTA';}
		if(naduana == '51')	{retval = 'LAZARO CARDENAS';}
		if(naduana == '52')	{retval = 'MONTERREY';}
		if(naduana == '53')	{retval = 'CANCUN';}
		if(naduana == '64')	{retval = 'QUERETARO';}
		if(naduana == '65')	{retval = 'TOLUCA';}
		if(naduana == '67')	{retval = 'CHIHUAHUA';}
		if(naduana == '73')	{retval = 'AGUASCALIENTES';}
		if(naduana == '75')	{retval = 'PUEBLA';}
		if(naduana == '80')	{retval = 'COLOMBIA';}
		if(naduana == '81')	{retval = 'ALTAMIRA';}
		if(naduana == '82')	{retval = 'CIUDAD CAMARGO';}
		if(naduana == '83')	{retval = 'DOS BOCAS, PARAISO';}
		if(naduana == '84')	{retval = 'GUANAJUATO';}
	
	return retval;
}


function isNull(value) {
    return (value == null) ? '' : value;
}

function isNull2(value, replaceby) {
    return (value == null) ? replaceby : value;
}

function LTrim(s) {
	
	var retval = "";
	
	if(s != null && s != "")
	{
		// Devuelve una cadena sin los espacios del principio
		var i = 0;
		var j = 0;
	
		// Busca el primer caracter <> de un espacio
		for (i = 0; i <= s.length - 1; i++)
			if (s.substring(i, i + 1) != ' ' && s.substring(i, i + 1) != '') {
			j = i;
			break;
		}
		
		retval =  s.substring(j, s.length);
	}
	
	return retval;
}
function RTrim(s) {
	
	var retval = "";
	
    // Quita los espacios en blanco del final de la cadena
	
	if(s != null && s != "")
	{
		var j = 0;
	
		// Busca el �ltimo caracter <> de un espacio
		for (var i = s.length - 1; i > -1; i--)
			if (s.substring(i, i + 1) != ' ' && s.substring(i, i + 1) != '') {
			j = i;
			break;
		}
		
		retval = s.substring(0, j + 1);
	}
	
	return retval;
}
function Trim(s) {
    // Quita los espacios del principio y del final
    return LTrim(RTrim(s));
}


/*function getTipoDocumento(recordtype) {
    var retval = '';
nlapiLogExecution("DEBUG", "recordtype:  ", recordtype);
    switch (recordtype) {
        
        case '10':
            retval = 'NOTA_DE_CREDITO'
            break;
		case  '7':
			retval = 'FACTURA';
			break;
    }
nlapiLogExecution("DEBUG", "retval :  ", retval );
    return retval;
}*/

function replace(texto, s1, s2) {
	try{
    	return texto.split(s1).join(s2);
	}
	catch(err)
	{
		
	}
}


function getTipoDocumento(recordtype) {
    var retval = '';

    switch (recordtype) {
        
        case 'CustCred':
            retval = 'NOTA_DE_CREDITO'
            break;
		case  'CustInvc':
			retval = 'FACTURA';
			break;
    }

    return retval;
}
