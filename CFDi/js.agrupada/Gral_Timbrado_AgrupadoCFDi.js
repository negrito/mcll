//===================================================================================================================================
// Script File	: Gral_Timbrado_AgrupadoCFDi.js
// Script Type  : Libreria
// Description 	:
// Author		: Ivan Gonzalez G. - Netsoft
// Date			: 18-09-2013
// 				: 
//===================================================================================================================================
//nlapiLogExecution("DEBUG", "idinvoice: ", idinvoice);


var CUSTOM_ERROR_CODE = 'ERRORMULTIELECTRONICTIMBRAR';
function TimbrarCFDi(wprocessid, wtype, subsidiary)
{
	var id 			= wprocessid;
	var record     	= nlapiLoadRecord('customrecord_process_massivecfdi', id);
	var fecha       = record.getFieldValue('custrecord_proc_cfdi_masiv_date');
	var setupcfdi 	= nlapiLoadRecord("customrecord_setup_cfdi", 1);
	var p_type      = wtype;
	if (fecha != null && fecha != '')
	{
		//esto es por si se seleccionó como filtro una solo transacción
		for(var  h = 1; h <= record.getLineItemCount("recmachcustrecord_dpfm_parent"); h++)
		{
			var idinvoice = record.getLineItemValue("recmachcustrecord_dpfm_parent" ,'custrecord_dpfm_ids' , h)
		}
		
		var invoice     = nsoGetTranRecord(idinvoice);
		var tranid      = invoice.getFieldValue("tranid");
		var subsidiary  = invoice.getFieldValue("subsidiary");
		var location    = invoice.getFieldValue("location");
	}
	
	
	var oneworld    	= setupcfdi.getFieldValue('custrecord_cfdi_oneworld');
	var billforlocation = setupcfdi.getFieldValue('custrecord_cfdi_location_testing');	
	var testing     	= setupcfdi.getFieldValue('custrecord_cfdi_testing'); 		
	var testingmasiva	= setupcfdi.getFieldValue('custrecord_cfdi_testing_masiva');
	var path        	= setupcfdi.getFieldValue('custrecord_cfdi_path') + "\\" + wprocessid + ".xml";	
	//esta es la url externa del script Gral|Masive CFDi | XML Generation
	var URLExterna  	= setupcfdi.getFieldValue('custrecord_cfdi_url_externa');
	//la url del scrip que hace el proceso de forma manual con los botones en la transacción
	var URLCFDi			= setupcfdi.getFieldValue('custrecord_cfdi_usrcfdi_externa');
	var setup       	= '';
	var bodytext 		= "";
	var docXml      	= true;
	var wtype           = (p_type == 'CustInvc') ? 7 : 10;
	
	if(oneworld == 'T' ){ 
	
		if(billforlocation == 'T'){
			setup = nlGetSetupRecord(subsidiary, location);
		}
		else {
			setup = nlGetSetupRecord(subsidiary, null);
		}	
	}
	else {
		
		if(billforlocation == 'T'){
			setup = nlGetSetupRecord(null, location);
		}
		else {
			setup = nlGetSetupRecord(null , null);
		}	
	}
	var base = nlapiGetContext().getEnvironment();	
	if(testingmasiva == 'T' || base == 'SANDBOX' || base == 'BETA')	{
		var idnameUserName = nlapiGetContext().getSetting('SCRIPT','custscript_cfdi_idfield_username_testing');
		var sRequestor  = setupcfdi.getFieldValue('custrecord_cfdi_testrequestor');
		var sEntity		= setupcfdi.getFieldValue('custrecord_cfdi_entity_testing');
		var sUser 		= setupcfdi.getFieldValue('custrecord_cfdi_testrequestor');	
		if(idnameUserName != null && idnameUserName != ''){
			var sUserName   = setupcfdi.getFieldValue(idnameUserName);
		}
		else{
			var sUserName   = setupcfdi.getFieldValue('custrecord_cfdi_username_testing');
		}
	}
	else {	
		var sRequestor  = setup.getFieldValue('custrecord_cfdi_requestor');
		var sEntity		= setup.getFieldValue('custrecord_cfdi_entity');
		var sUser 		= setup.getFieldValue('custrecord_cfdi_user');	
		var sUserName   = setup.getFieldValue('custrecord_cfdi_username');
	}
	
	try
	{
		if (idinvoice != null && idinvoice != ''){
			nlapiLogExecution("DEBUG", "idinvoice != null && idinvoice != '': ", idinvoice);
			var url       = URLCFDi + "&invoiceid=" + idinvoice+ "&wtype=" + wtype;}
		else{
		nlapiLogExecution("DEBUG", "wprocessid: ", wprocessid);	
		var url = URLExterna + "&wprocessid=" + wprocessid + "&wtype=" + wtype + "&wsetup=" + setup.getId() ;}
		
		nlapiLogExecution("DEBUG", "url: ", url);
		var objResponse = nlapiRequestURL(url, null, null, null);
		bodytext = objResponse.getBody();
		
		docXml =  nsoSendToWS(0, bodytext, 'XML PDF', id, wtype, sRequestor, sEntity, sUser, sUserName);
		if (docXml == null || docXml == ''){
			nlapiSubmitField('customrecord_process_massivecfdi', wprocessid, "custrecord_proc_cfdi_masiv_status", 3);
		}
	}
	catch (ex)
	{
		nlapiLogExecution('ERROR', ex instanceof nlobjError ? ex.getCode() : CUSTOM_ERROR_CODE,
		ex instanceof nlobjError ? ex.getDetails() : 'JavaScript Error: ' + (ex.message != null ? ex.message : ex));
	}
	
}

function nlGetSetupRecord(subsidiary, location)
{
	var retval  = null;	
	var index   = 0;
	
	var filters = new Array();
	
	if(subsidiary != null && subsidiary != "") {
		filters[index]  = new nlobjSearchFilter("custrecord_cfdi_subsidiary", null, "anyof", subsidiary);
		index += 1;
	}
	
	if(location != null && location != "") {
		filters[index]  = new nlobjSearchFilter("custrecord_cfdi_location", null, "anyof", location);
		index += 1;
	}
		
	var results = nlapiSearchRecord("customrecord_cfdisetup", null, filters, null);	
	
	if(results != null && results.length > 0)
	{
		retval = nlapiLoadRecord(results[0].getRecordType(), results[0].getId()) ;
	}
	else
	{
		var msg = "No se encontró el registro de configuración de la factura electrónica para la ubicación o subsidiaria";
		throw nlapiCreateError("ERROR", msg, false);
	}
	
	return retval;
}


function nsoGetTranRecord(id, dynamic)
{
	var itemrecord = null;

	if (id != null && id != "")
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('internalid', null, 'is', id);
		filters[1] = new nlobjSearchFilter('mainline', null, 'is', "T");		
		var searchresults = nlapiSearchRecord('transaction', null, filters, null);
		
		if (searchresults != null && searchresults.length > 0)
		{
			itemrecord = nlapiLoadRecord(searchresults[0].getRecordType(), searchresults[0].getId(), dynamic == true ? {recordmode:'dynamic'} :  null);
		}
	}
	
	return itemrecord;
}
