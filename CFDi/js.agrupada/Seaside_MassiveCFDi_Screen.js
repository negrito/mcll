//===================================================================================================================================
// Script File	: Seaside_MassiveCFDi_Screen.js
// Script Type  : Suitelet
// Description 	:
// Author		: Ivan Gonzalez G. - Netsoft
// Date			: 19-08-2013
// 				: Genera pantalla para facturación electrónica masiva.
// Dependencias : Registro "CFDi|Massive Process|Gral" con id "customrecord_process_massivecfdi"
//===================================================================================================================================

var CUSTOM_ERROR_CODE = 'ERRORMULTIELECTRONIC';
//nlapiLogExecution('DEBUG', 'subsidiary en nlDisplayRequest: ',subsidiary);	

function ShowInvoicesToProcess(request, response)
{
	if (request.getMethod() == "GET"){
		var form     = nlShowRequestForm(request, response);
		try{
			nlDisplayRequest(request, response, form);	
		}
		catch (ex){
			nlapiLogExecution('ERROR', ex instanceof nlobjError ? ex.getCode() : CUSTOM_ERROR_CODE,
			ex instanceof nlobjError ? ex.getDetails() : 'JavaScript Error: ' + (ex.message != null ? ex.message : ex));
		}
	}

	if (request.getMethod() == "POST"){
		var idrecord = ProcessMassiveInvoice(request, response);		
	}			
}




function nlShowRequestForm(request, response)
{
	var dateini 	    = request.getParameter("wdateini");
	var dateend		    = request.getParameter("wdateend");	
	var wtype 			= request.getParameter("wtype");
	var entity  		= request.getParameter("entity");
	var subsidiary		= request.getParameter("wsubsidiary");
	var location		= request.getParameter("wlocation");
	var currency		= request.getParameter("wcurrency");
	var showsubsidiary  = nlapiGetContext().getSetting('SCRIPT','custscript_display_subsidiary');
	var showcliente     = nlapiGetContext().getSetting('SCRIPT','custscript_display_cliente');
	var showlocation    = nlapiGetContext().getSetting('SCRIPT','custscript_display_location');
	var showcurrency    = nlapiGetContext().getSetting('SCRIPT','custscript_display_currency');
	var formapagobase   = nlapiGetContext().getSetting('SCRIPT','custscript_display_formapago_base')||'';
	var metpagobase     = nlapiGetContext().getSetting('SCRIPT','custscript_display_metpago_base')||'';
	var numSub          = '';
	var nameSub         = '';
	var metpago  		= request.getParameter("wmetpago");
	var usodcfdi		= request.getParameter("wusocfdi");
	
	if (showsubsidiary  != null && showsubsidiary != ''){
		var subsi   = new Array();
		subsi       = showsubsidiary.split('|');
		var numSub  = subsi[0];
		var nameSub = subsi[1];
	}
	//--> Add Buttons
	var form = nlapiCreateForm('Factura Electrónica Masiva');
	var group = form.addFieldGroup( 'myfieldgroup', 'Tipo de Transacción');
	var fieldtype = form.addField('custpage_type','select', 'Tipo Transacci' +  String.fromCharCode(243) + 'n', null, 'myfieldgroup');	
	fieldtype.addSelectOption('CustInvc', 'Factura');
	fieldtype.addSelectOption('CustCred', 'Nota de Cr' + String.fromCharCode(233) + 'dito');
	fieldtype.setDefaultValue(wtype);
	group.setShowBorder(true);

	var group = form.addFieldGroup( 'myfieldgroup2', 'Filtros');
	field = form.addField('custpage_datestart','date', 'Fecha Inicio', null , 'myfieldgroup2');
	field.setDefaultValue(dateini);
	field = form.addField('custpage_dateend','date', 'Fecha Fin', null, 'myfieldgroup2');
	field.setDefaultValue(dateend);

	field = form.addField('custpage_entity','select', 'Cliente', 'customer', 'myfieldgroup2');
	field.setDefaultValue(entity);
	if (showcliente != 'T'){
		field.setDisplayType("hidden");
	}
	//field.setDisplayType("hidden");
	field = form.addField('custpage_currency','select', 'Moneda', 'currency', 'myfieldgroup2');
	field.setDefaultValue(currency);
	if (showcurrency != 'T'){
		field.setDisplayType("hidden");
	}
	field = form.addField('custpage_usocfdi','select', 'Uso del CFDi', 'customlist_cfdi_uso', 'myfieldgroup2');
	field.setDefaultValue(usodcfdi);
	field.setMandatory(true);
	
	if (numSub != null && numSub != '' && nameSub != null && nameSub != ''){
		field = form.addField('custpage_subsidiary','select', 'Subsidiaria',null, 'myfieldgroup2');
		field.addSelectOption(numSub, nameSub);
		field.setDefaultValue(subsidiary);
	}
	else{
		field = form.addField('custpage_subsidiary','select', 'Subsidiaria', 'subsidiary', 'myfieldgroup2');
		field.setDefaultValue(subsidiary);
	}
	
	field = form.addField('custpage_location','select', 'Ubicacion', 'location', 'myfieldgroup2');
	field.setDefaultValue(location);
	if (showlocation != 'T'){
		field.setDisplayType("hidden");
	}
	
	field = form.addField('custpage_formapago','select', 'Forma de Pago', 'paymentmethod', 'myfieldgroup2');
	field.setDefaultValue(FormadePago);
	field.setMandatory(true);
	field = form.addField('custpage_metpago','select', 'Metodo de Pago', 'customlist_cfdi_formapago', 'myfieldgroup2');
	field.setDefaultValue(MetodoPago);
	field.setMandatory(true);
	
	group.setShowBorder(true);
	
	
	var field = form.addField('custpage_proceso', 'text', '.......', null, 'myfieldgroup2');
	field.setDisplayType("inline");

	
	//----> Add script client
	form.setScript('customscript_cfdi_agrupada_client');

	//--> Add Buttons
	form.addButton("custpage_btnsearch", "Filtrar", "showRequest();");
	//form.addButton("custpage_btncrefile", "Ejecutar", "ejecutar");
	form.addSubmitButton('Generacion Factura Electronica');
	
	
	//----> Add sublist
	var sublist = form.addSubList('custpage_invoice_list','list','Facturas');
	sublist.addMarkAllButtons();
	var fld = sublist.addField('custpage_marked','checkbox', 'Aplicar');	
	
	fld = sublist.addField('custpage_invoice','select', 'Transacci' +  String.fromCharCode(243) + 'n', 'invoice');	
	fld.setDisplayType("inline");	
	fld = sublist.addField('custpage_trandate','text', 'Fecha');	
	fld.setDisplayType("inline");
	fld = sublist.addField('custpage_xitemname','select', 'Cliente', 'customer');	
	fld.setDisplayType("inline");
	fld = sublist.addField('custpage_total','text', 'Total');	
	fld.setDisplayType("inline");

	
	//--> Write Page
	response.writePage( form );
	
	return form;		
}
	
	
function nlDisplayRequest(request, response, form)
{	
	var dateini 		= request.getParameter("wdateini");
	var dateend 		= request.getParameter("wdateend");
	var wtype 			= request.getParameter("wtype");	
	var entity  		= request.getParameter("entity");	
	var subsidiary		= request.getParameter("wsubsidiary");	
	var location		= request.getParameter("wlocation");
	var currency		= request.getParameter("wcurrency");		
	
	if (subsidiary != null && subsidiary != ''){
		if ((dateini != null && dateini != '' && dateend != null && dateend != '')||(entity != null && entity != '')){
			var linenum      = 0;	
			var searchresult = nsoGetAllInvoice(wtype, dateini, dateend, entity, subsidiary, location, currency);
			 
			if(searchresult){
				var sublist = form.getSubList("custpage_invoice_list");								
					
				for(var i = 0 ; searchresult != null && i < searchresult.length; i++){						
					var wid          = searchresult[i].getId();
					var xitemname    = searchresult[i].getValue("entity");			
					var trandate     = searchresult[i].getValue("trandate");	
					var tranid       = searchresult[i].getValue("tranid");
					var taxtotal     = searchresult[i].getValue("taxtotal");
					var total        = searchresult[i].getValue("fxamount");
					
					linenum += 1;
					nlapiInsertLineItem('custpage_invoice_list', linenum );		
					sublist.setLineItemValue('custpage_marked', linenum , 'T');
					sublist.setLineItemValue('custpage_invoice', linenum , wid);			
					sublist.setLineItemValue('custpage_xitemname', linenum, xitemname);
					sublist.setLineItemValue('custpage_trandate', linenum, trandate);	
					//sublist.setLineItemValue('custpage_tranid', linenum, tranid);
					sublist.setLineItemValue('custpage_taxtotal', linenum, total);												
				}//end for(var i = 0 ; searchresult != null && i < searchresult.length; i++){	
			}//end if(searchresult){ 
		}//end if ((dateini != null && dateini != 
	}//end if (subsidiary != null && subsidiary != ''){
}


function ProcessMassiveInvoice(request, response)
{	
	var wdateini   = request.getParameter('custpage_datestart');
	var wdateend   = request.getParameter('custpage_dateend');		
	var wtype      = request.getParameter('custpage_type');
	var idtran     = request.getParameter("custpage_transaction");
	var entity     = request.getParameter("custpage_entity");
	var subsidiary = request.getParameter("custpage_subsidiary");
	var location   = request.getParameter("custpage_location");
	var currency   = request.getParameter("custpage_currency");
	var MetodoPago = request.getParameter('custpage_metpago');
	var usodcfdi   = request.getParameter("custpage_usocfdi");
	var FormaPago  = request.getParameter("custpage_formapago");
	var idprocess = ProcElectronicInvoiceMassive(wdateini, wdateend, wtype, idtran, entity, subsidiary, location, currency, MetodoPago, usodcfdi, FormaPago);
	//nlapiLogExecution('DEBUG', 'idprocess : ', idprocess); 
	if (idprocess != null && idprocess != ''){
		nlapiSetRedirectURL("RECORD", "customrecord_process_massivecfdi", idprocess, null, null);  
		//timbrado del registro customizado
		try	{
			TimbrarCFDi(idprocess, wtype, subsidiary);	
			nlapiSubmitField('customrecord_process_massivecfdi', idprocess, "custrecord_proc_cfdi_masiv_end", nlapiDateToString(new Date(), "datetime"));
		}
		catch (ex){
			nlapiLogExecution('ERROR', ex instanceof nlobjError ? ex.getCode() : CUSTOM_ERROR_CODE,
			ex instanceof nlobjError ? ex.getDetails() : 'JavaScript Error: ' + (ex.message != null ? ex.message : ex));
		}
	
		//--> Launch Process que marca las facturas como procesadas y les fija el ID del registro generado
	
		/*var record 	= nlapiLoadRecord('customrecord_process_massivecfdi', idprocess);
		var xml     = record.getFieldValue('custrecord_proc_cfdi_masiv_xml');
		
		if (xml != null && xml != ''){
			var params = new Array();				
			params['custscript_id_to_assign_gral']  = idprocess;
			//nlapiLogExecution('DEBUG', 'idprocess: ' , idprocess);	
			wstatus = nlapiScheduleScript('customscript_mass_cfdi_scheduled', 'customdeploy_mass_cfdi_scheduled', params);	
			
			if( wstatus == "QUEUED" ){
				nlapiSetRedirectURL("RECORD", "customrecord_process_massivecfdi", idprocess, null, null);
			}															
			else{
				wstatus1 = nlapiScheduleScript('customscript_mass_cfdi_scheduled', 'customdeploy_mass_cfdi_scheduled_1', params);
				if( wstatus1 == "QUEUED" ){
					nlapiSetRedirectURL("RECORD", "customrecord_process_massivecfdi", idprocess, null, null);
				}		
				else{
					wstatus2 = nlapiScheduleScript('customscript_mass_cfdi_scheduled', 'customdeploy_mass_cfdi_scheduled_2', params);
					if( wstatus2 == "QUEUED" ){
						nlapiSetRedirectURL("RECORD", "customrecord_process_massivecfdi", idprocess, null, null);
					}		
					else{
						throw nlapiCreateError("ERROR", "Hay procesos pendientes de ejecución, espere a que alguno termine para iniciar un nuevo proceso!!!", false);
					}
				}
			}	
		}*/
	}//end if (idprocess != null && idprocess != '')
}
				
				
				

function nsoParseFloatOrZero(f)
{
   var r=parseFloat(f);
   return isNaN(r) ? 0 : r;
}

function nsoParseIntOrZero(i)
{
   var r=parseInt(i);
   return isNaN(r) ? 0 : r;
}

function nsoGetAllInvoice(wtype, dateini, dateend, entity, subsidiary, p_location, p_currency)
{	
	var setupcfdi       = nlapiLoadRecord("customrecord_setup_cfdi", 1); 
	var testingmasiva   = setupcfdi.getFieldValue('custrecord_cfdi_testing_masiva');
	var oneworld        = setupcfdi.getFieldValue('custrecord_cfdi_oneworld');
	var filters = new Array();
	filters.push ( new nlobjSearchFilter('mainline', null, 'is', 'T'));
	filters.push ( new nlobjSearchFilter('type', null, 'anyof', wtype));
	filters.push ( new nlobjSearchFilter('memorized', null, 'is', 'F'));
	
	if (oneworld == 'T'){
	filters.push ( new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary));}
	
	if(testingmasiva != 'T'){
		filters.push ( new nlobjSearchFilter('custbody_massive_processid', null, 'anyof', '@NONE@'));
		filters.push ( new nlobjSearchFilter('custbody_cfdixml', null, 'isempty'));
	}
	if(dateini != null && dateini != "" && dateend != null && dateend != ""){
		filters.push ( new nlobjSearchFilter('trandate', null, 'within', dateini, dateend));
	}
	if (entity != null && entity != ''){
		filters.push ( new nlobjSearchFilter('entity', null, 'anyof', entity));
	}
	if (p_location != null && p_location != ''){
		filters.push ( new nlobjSearchFilter('location', null, 'anyof', p_location));
	}
	if (p_currency != null && p_currency != ''){
		filters.push ( new nlobjSearchFilter('currency', null, 'anyof', p_currency));
	}
	
	var columns  = new Array();	
	columns[0]   = new nlobjSearchColumn("trandate");	
	columns[1]   = new nlobjSearchColumn("entity");	
	columns[2]   = new nlobjSearchColumn("tranid");
	columns[3]   = new nlobjSearchColumn("taxtotal");
	
	//-----> Carga busqueda guardada y fija filtros y columnas		
	//var mySearch  = nlapiLoadSearch('transaction', 'customsearch_invoicemassive');
	var mySearch = nlapiSearchRecord('transaction', 'customsearch_invoicemassive', filters , null);
	/*mySearch.setFilters(filters);
	mySearch.setColumns(columns)	
	var resultSet = mySearch.runSearch();
	var searchresults = new Array();
	//nlapiLogExecution('DEBUG', 'resultSet.length en nsoGetAllInvoice: ', resultSet.length); 
    //----> Ciclo para traer hasta 10,000 registros
	for(var i = 0 ; resultSet != null && i < 10; i++)		
	{
		var indxini   = nsoParseIntOrZero(1000 * i);  
		var indxend   = nsoParseIntOrZero(indxini) + 1000;  
		var results   = resultSet.getResults(indxini, indxend);

		if(results)
		{
			searchresults = searchresults.concat(results);
		}
       else
       	break;		
	   			
	}
	
	return searchresults;*/
	return mySearch;
}
