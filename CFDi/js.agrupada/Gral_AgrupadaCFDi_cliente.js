//===================================================================================================================================
// Script File	: Gral_AgrupadaCFDi_cliente.js
// Script Type  : Cliente
// Description 	:
// Author		: Ivan Gonzalez G. - Netsoft
// Date			: 15-07-2016
// 				: proceso para facturación electrónica masiva.
//===================================================================================================================================

//--> On Click-Button Event
function InvoiceAgrupada_FieldChanged(type, name, numline)
{	
	if(name == 'custpage_datestart' || name == 'custpage_dateend' || name == 'custpage_entity' || name == 'custpage_currency' || name == 'custpage_subsidiary')
	{		
		var wdateini    = nlapiGetFieldValue('custpage_datestart');
		var wdateend    = nlapiGetFieldValue('custpage_dateend');		
		var wtype       = nlapiGetFieldValue('custpage_type');
		var idtran      = nlapiGetFieldValue("custpage_transaction");
		var entity      = nlapiGetFieldValue("custpage_entity");
		var subsidiary  = nlapiGetFieldValue("custpage_subsidiary");
		var location    = nlapiGetFieldValue("custpage_location");
		var currency    = nlapiGetFieldValue("custpage_currency");
		var metpago     = nlapiGetFieldValue('custpage_metpago');
		var usodcfdi	= nlapiGetFieldValue("custpage_usocfdi");
		var formapago	= nlapiGetFieldValue("custpage_formapago");
		
		var url         = nlapiResolveURL('SUITELET', 'customscript_cfdi_agrupado_screen', 'customdeploy_cfdi_agrupado_screen') + '&wdateini=' + wdateini + '&wdateend=' + wdateend  +'&wtype=' + wtype + '&idtrans=' + idtran + '&entity=' + entity + '&wsubsidiary=' + subsidiary + '&wlocation=' + location + '&wcurrency=' + currency  + '&wmetpago' + metpago + '&wusocfdi' + usodcfdi + '&wformapago' + formapago;	
		window.ischanged = false;
		window.location = url;
	}
}

function showRequest()
{
	var wdateini    = nlapiGetFieldValue('custpage_datestart');
	var wdateend    = nlapiGetFieldValue('custpage_dateend');		
	var wtype       = nlapiGetFieldValue('custpage_type');
	var idtran      = nlapiGetFieldValue("custpage_transaction");
	var entity      = nlapiGetFieldValue("custpage_entity");
	var subsidiary  = nlapiGetFieldValue("custpage_subsidiary");
	var location    = nlapiGetFieldValue("custpage_location");
	var currency    = nlapiGetFieldValue("custpage_currency");
	var metpago     = nlapiGetFieldValue('custpage_metpago');
	var usodcfdi	= nlapiGetFieldValue("custpage_usocfdi");
	var formapago	= nlapiGetFieldValue("custpage_formapago");
		
	var url         = nlapiResolveURL('SUITELET', 'customscript_cfdi_agrupado_screen', 'customdeploy_cfdi_agrupado_screen') + '&wdateini=' + wdateini + '&wdateend=' + wdateend  +'&wtype=' + wtype + '&idtrans=' + idtran + '&entity=' + entity + '&wsubsidiary=' + subsidiary + '&wlocation=' + location + '&wcurrency=' + currency  + '&wmetpago' + metpago+ '&wusocfdi' + usodcfdi + '&wformapago' + formapago;	
	window.ischanged = false;
	window.location = url;
}


