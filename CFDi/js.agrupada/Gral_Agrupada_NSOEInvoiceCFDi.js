//===================================================================================================================================
// Company        : NetSoft, www.netsoft.com.mx
// Name of Script : Agrupada_NSOEInvoiceCFDi.js
// Author         : Ivan Gonzalez - Netsoft - M�xico
// Date			  : 12-09-2013
// Base			  : McGeever
// Type           : SuiteLet
// Sub-Type       : Business Process
// Categories     : Business logic
// Description    : Return text with layout to an invoice/memo-credit
// NetSuite Ver.  : (Edition: International) Release 2010
// Installation   :   SuiteLet                  Method                ScriptId           DeploymentId
//                    ------------------------- -----------------     ------------------ ----------------
//		              CFDi		   				nsoEInvoiceCFD        
//
// Parameters     :   Name             Description                            Type      Value(s)
//                    ---------------- -------------------------------------- --------- -----------
//                    invoiceid        Internalid of invoice or memocredit    integer   
//
// Dependences     : File Name
//                   --------------------
//                   NSOUtils.js
//                   ObjEInvoiceCFDi.js
//                   NSOCountriesStates.js
//===================================================================================================================================

function nsoEInvoiceCFDi(request, response)
{
	//nlapiLogExecution('DEBUG', 'entro : nsoEInvoiceCFD', 'nsoEInvoiceCFD'); 
	var invoiceid 	 = request.getParameter('wprocessid');
	var setupid      = request.getParameter('wsetup');
	var wtype        = request.getParameter('wtype');
	nlapiLogExecution('DEBUG', 'invoiceid', invoiceid); 
	nlapiLogExecution('DEBUG', 'setup', setupid); 
	nlapiLogExecution('DEBUG', 'wtype', wtype); 
	//var setupid      = nlGetSetupEInvoiceId(invoiceid, subsidiary);
	var objEInvoice  = new NSObjEInvoiceCFDiAgrupado();
	var text         = objEInvoice.build(invoiceid, setupid, wtype);
	response.write(text);	
}


