//=================================================================================================================================
// Script File	 : Gral_Agrupada_Buttons.js
// Script Type   : User
// Author		 : Ivan Gonzalez - Netsoft
// Date			 : 13-10-2015
// Base			 : DEV
//=================================================================================================================================
var CUSTOM_ERROR_CODE = 'ERRORMULTIELECTRONIC';

function nsoAddButtonPrintInvoiceAgrupada(type, form, request)
{
	if(type == 'edit'){
		var wid     = nlapiGetRecordId();					
		var xtype   = nlapiGetRecordType();
		var xml = nlapiGetFieldValue('custrecord_proc_cfdi_masiv_xml');
		var cancelada = nlapiGetFieldValue('custrecord_proc_cfdi_masiv_nocanceled')||'F';
		if(xml == null || xml == ''){
			form.addButton("custpage_btnCFDi", "Genera CFDi", "click_cfdi_agrupado()"); 	
		}
		if(xml != null && xml != ''){
			var script  = "window.location = '" + nlapiResolveURL('SUITELET', 'customscript_gral_agrup_print', 'customdeploy_gral_agrup_print') + '&idinvoice=' + wid +  '&wtype=' + xtype + "';";
			form.addButton('custpage_btnprintcfdi', 'Imprimir CFDi', script);
			if (cancelada != 'T'){
				form.addButton("custpage_btncancelcfd", "Cancelar CFDi", "click_cancelcfdiAgrupado()");
			}
		}
	}
}


//--> On Init-Page
function nsoDoNothing(){}


//--> On Click-Button Event
function click_cancelcfdiAgrupado()
{
	var wtype       = nlapiGetRecordType();
	var id  		= nlapiGetFieldValue('id');
	var invoice     = nlapiLoadRecord(wtype, id);
	var subsidiary  = invoice.getFieldValue("custrecord_proc_cfdi_masiv_subsidiaary");
	var runprocess  = nlapiGetContext().getSetting('SCRIPT','custscript_cfdi_adrupadi_descheck');
	var wxml 		= nlapiStringToXML(invoice.getFieldValue("custrecord_proc_cfdi_masiv_xml"));
    
	var UUID        = '';
	if (wxml){
	    var Comprobante     = nlapiSelectNodes(wxml, "//*" );	
		var folio           = isNull(Comprobante[0].getAttribute('folio'));
		var serie           = isNull(Comprobante[0].getAttribute('serie'));
		var Complemento     = nlapiSelectNodes(wxml ,"//*[name()='tfd:TimbreFiscalDigital']" ); 
		var UUID            = isNull(Complemento[0].getAttribute('UUID'));
	}
	var setupcfdi   = nlapiLoadRecord("customrecord_setup_cfdi", 1);
	var oneworld    = setupcfdi.getFieldValue('custrecord_cfdi_oneworld')||'';
	var testing     = setupcfdi.getFieldValue('custrecord_cfdi_testing_masiva')||''; 	
	var billforlocation = setupcfdi.getFieldValue('custrecord_cfdi_location_testing')||'';	
	
	
	if(oneworld == 'T' ){ 
		if(billforlocation == 'T'){
			setup = nlGetSetupRecord(subsidiary, location);
		}
		else {
			setup = nlGetSetupRecord(subsidiary, null);
		}	
	}
	else {
		if(billforlocation == 'T'){
			setup = nlGetSetupRecord(null, location);
		}
		else {
			setup = nlGetSetupRecord(null , null);
		}	
	}
	var base = nlapiGetContext().getEnvironment();
	
	if(testing == 'T' || base == 'SANDBOX' || base == 'BETA')	{
		var sRequestor  = setupcfdi.getFieldValue('custrecord_cfdi_testrequestor');
		var sEntity		= setupcfdi.getFieldValue('custrecord_cfdi_entity_testing');
		var sUser 		= setupcfdi.getFieldValue('custrecord_cfdi_testrequestor');	
		var sUserName   = setupcfdi.getFieldValue('custrecord_cfdi_username_testing');
	}
	else {	
		var sRequestor  = setup.getFieldValue('custrecord_cfdi_requestor');
		var sEntity		= setup.getFieldValue('custrecord_cfdi_entity');
		var sUser 		= setup.getFieldValue('custrecord_cfdi_user');	
		var sUserName   = setup.getFieldValue('custrecord_cfdi_username');
	}
	
	if(confirm('Esta por Cancelar la Factura No. : ' + serie + folio + ', Desea Continuar?') ==  true){
		try{
			if (UUID != null && UUID != ''){
				serie = UUID;
				folio = '';
			}
			var lResult = nsoSendToWS(1,serie, folio , id, wtype, sRequestor, sEntity, sUser, sUserName);
			if(testing == 'T' || base == 'SANDBOX' || base == 'BETA')	{
				lResult = '';
			}
			if (lResult == null || lResult == ''){
				nlapiSubmitField(wtype, id, 'custrecord_proc_cfdi_masiv_cancel', 'T');
				alert('Proceso de Cancelacion Terminado. Salve/Guarde el registro.');
				window.location = window.location;
			}
			else{
				alert('No se pudo cancelar.');
			}
		}
		catch(e)
		{
			alert(e.description);
			return;
		}
	}
}


function nlGetSetupRecord(subsidiary, location)
{
	var retval  = null;	
	var index   = 0;
	
	var filters = new Array();
	
	if(subsidiary != null && subsidiary != "") {
		filters[index]  = new nlobjSearchFilter("custrecord_cfdi_subsidiary", null, "anyof", subsidiary);
		index += 1;
	}
	
	if(location != null && location != "") {
		filters[index]  = new nlobjSearchFilter("custrecord_cfdi_location", null, "anyof", location);
		index += 1;
	}
		
	var results = nlapiSearchRecord("customrecord_cfdisetup", null, filters, null);	
	
	if(results != null && results.length > 0){
		retval = nlapiLoadRecord(results[0].getRecordType(), results[0].getId()) ;
	}
	else{
		var msg = "No se encontró el registro de configuración de la factura electrónica para la ubicación o subsidiaria";
		throw nlapiCreateError("ERROR", msg, false);
	}
	
	return retval;
}

function isNull(value) {
    return (value == null) ? '' : value;
}

function click_cfdi_agrupado()
{
	var id 			= nlapiGetFieldValue("id");	
	var wprocessid  = id;
	var record     	= nlapiLoadRecord('customrecord_process_massivecfdi', id);
	var wtype       = record.getFieldValue('custrecord_proc_cfdi_masiv_type');
	var fecha       = record.getFieldValue('custrecord_proc_cfdi_masiv_date');
	var subsidiary  = record.getFieldValue("custrecord_proc_cfdi_masiv_subsidiaary");
	var setupcfdi 	= nlapiLoadRecord("customrecord_setup_cfdi", 1);
	var p_type      = wtype;
	var base        = nlapiGetContext().getEnvironment();
	if (fecha != null && fecha != '')
	{
		//esto es por si se seleccionó como filtro una solo transacción
		for(var  h = 1; h <= record.getLineItemCount("recmachcustrecord_dpfm_parent"); h++)
		{
			var idinvoice = record.getLineItemValue("recmachcustrecord_dpfm_parent" ,'custrecord_dpfm_ids' , h)
		}
		
		var invoice     = nsoGetTranRecord(idinvoice);
		var tranid      = invoice.getFieldValue("tranid");
		var subsidiary  = invoice.getFieldValue("subsidiary");
		var location    = invoice.getFieldValue("location");
	}
	
	
	var oneworld    	= setupcfdi.getFieldValue('custrecord_cfdi_oneworld');
	var billforlocation = setupcfdi.getFieldValue('custrecord_cfdi_location_testing');	
	var testing     	= setupcfdi.getFieldValue('custrecord_cfdi_testing'); 		
	var testingmasiva	= setupcfdi.getFieldValue('custrecord_cfdi_testing_masiva');
	var path        	= setupcfdi.getFieldValue('custrecord_cfdi_path') + "\\" + id + ".xml";	
	//esta es la url externa del script Gral|Masive CFDi | XML Generation
	var URLExterna  	= setupcfdi.getFieldValue('custrecord_cfdi_url_externa');
	//la url del scrip que hace el proceso de forma manual con los botones en la transacción
	var URLCFDi			= setupcfdi.getFieldValue('custrecord_cfdi_usrcfdi_externa');
	var setup       	= '';
	var bodytext 		= "";
	var docXml      	= true;
	var wtype           = (p_type == 'CustInvc') ? 7 : 10;
	
	if(oneworld == 'T' ){ 
	
		if(billforlocation == 'T'){
			setup = nlGetSetupRecord(subsidiary, location);
		}
		else {
			setup = nlGetSetupRecord(subsidiary, null);
		}	
	}
	else {
		
		if(billforlocation == 'T'){
			setup = nlGetSetupRecord(null, location);
		}
		else {
			setup = nlGetSetupRecord(null , null);
		}	
	}
		
	if(testingmasiva == 'T' || base == 'SANDBOX' || base == 'BETA')	{
		var idnameUserName = nlapiGetContext().getSetting('SCRIPT','custscript_idfield_usarname');
		var sRequestor  = setupcfdi.getFieldValue('custrecord_cfdi_testrequestor');
		var sEntity		= setupcfdi.getFieldValue('custrecord_cfdi_entity_testing');
		var sUser 		= setupcfdi.getFieldValue('custrecord_cfdi_testrequestor');	
		if(idnameUserName != null && idnameUserName != ''){
			var sUserName   = setupcfdi.getFieldValue(idnameUserName);
		}else{
			var sUserName   = setupcfdi.getFieldValue('custrecord_cfdi_username_testing');
		}
	}
	else {	
		var sRequestor  = setup.getFieldValue('custrecord_cfdi_requestor');
		var sEntity		= setup.getFieldValue('custrecord_cfdi_entity');
		var sUser 		= setup.getFieldValue('custrecord_cfdi_user');	
		var sUserName   = setup.getFieldValue('custrecord_cfdi_username');
	}
	
	try{
		if (idinvoice != null && idinvoice != ''){
			nlapiLogExecution("DEBUG", "idinvoice != null && idinvoice != '': ", idinvoice);
			var url       = URLCFDi + "&invoiceid=" + idinvoice+ "&wtype=" + wtype;}
		else{
			nlapiLogExecution("DEBUG", "wprocessid: ", wprocessid);	
			var url = URLExterna + "&wprocessid=" + wprocessid + "&wtype=" + wtype + "&wsetup=" + setup.getId() ;}
			nlapiLogExecution("DEBUG", "url: ", url);
			var objResponse = nlapiRequestURL(url, null, null, null);
			bodytext = objResponse.getBody();
			nlapiSubmitField('customrecord_process_massivecfdi', id, 'custrecord_proc_cfdi_xml_test', bodytext);
			docXml =  nsoSendToWS(0, bodytext, '', id, wtype, sRequestor, sEntity, sUser, sUserName);
			window.location = window.location;
			if (docXml == null || docXml == ''){
				nlapiSubmitField('customrecord_process_massivecfdi', id, "custrecord_proc_cfdi_masiv_nota", '');
				nlapiSubmitField('customrecord_process_massivecfdi', id, "custrecord_proc_cfdi_masiv_status", 3);
				nlapiSubmitField('customrecord_process_massivecfdi', id, "custrecord_proc_cfdi_masiv_end", nlapiDateToString(new Date(), "datetime"));
				
		}
	}
	catch (ex)
	{
		nlapiLogExecution('ERROR', ex instanceof nlobjError ? ex.getCode() : CUSTOM_ERROR_CODE,
		ex instanceof nlobjError ? ex.getDetails() : 'JavaScript Error: ' + (ex.message != null ? ex.message : ex));
	}
	
}

function AsigId_BeforeSubmit (type)
{nlapiLogExecution("DEBUG", "type before: ", type);
	if(type == 'edit'){
		var record     = nlapiGetNewRecord();
		var oldRecord  = nlapiGetOldRecord();
		var runprocess = nlapiGetContext().getSetting('SCRIPT','custscript_cfdi_adrupadi_descheck');
		var xml        = record.getFieldValue('custrecord_proc_cfdi_masiv_xml')||'';
		var oldxml     = oldRecord.getFieldValue('custrecord_proc_cfdi_masiv_xml')||''
		var cancel     = record.getFieldValue( 'custrecord_proc_cfdi_masiv_cancel');
		var cancelold  = oldRecord.getFieldValue( 'custrecord_proc_cfdi_masiv_cancel');
		var id         = nlapiGetRecordId();
		var setupcfdi  = nlapiLoadRecord("customrecord_setup_cfdi", 1);
		var testingmasiva	= setupcfdi.getFieldValue('custrecord_cfdi_testing_masiva')||'F';
		if ((xml != oldxml && oldxml == '')||(testingmasiva == 'T')){
			if (xml != null && xml != ''){
				var params = new Array();				
				params['custscript_id_to_assign_gral']  = id;
				//nlapiLogExecution('DEBUG', 'idprocess: ' , idprocess);	
				wstatus = nlapiScheduleScript('customscript_mass_cfdi_scheduled', 'customdeploy_mass_cfdi_scheduled', params);	
				
				if( wstatus == "QUEUED" ){
					nlapiSetRedirectURL("RECORD", "customrecord_process_massivecfdi", id, null, null);
				}															
				else{
					throw nlapiCreateError("ERROR", "Hay procesos pendientes de ejecución, espere a que alguno termine para iniciar un nuevo proceso!!!", false);
				}	
			}
		}//if (xml != oldxml && oldxml == ''){
		
		if ((cancel == 'T' && cancelold == 'F')||(testingmasiva == 'T') && xml != null ){
			if(runprocess == 'T' && (cancel == 'T' && cancelold == 'F')){
				var params = new Array();				
				params['custscript_id_registro']    = nlapiGetRecordId();
				params['custscript_type_registro']  = nlapiGetRecordType();
				
				var wstatus = nlapiScheduleScript('customscript_mass_cfdi_cancel_sched', 'customdeploy_mass_cfdi_cancel_sched', params);	
				if( wstatus == "QUEUED" ){
					nlapiSetRedirectURL("RECORD", "customrecord_process_massivecfdi", id, null, null);
				}															
				else{
					var wstatus1 = nlapiScheduleScript('customscript_mass_cfdi_cancel_sched', 'customdeploy_mass_cfdi_cancel_sched_1', params);	
					if( wstatus1 == "QUEUED" ){
					nlapiSetRedirectURL("RECORD", "customrecord_process_massivecfdi", id, null, null);
					}															
					else{
						throw nlapiCreateError("ERROR", "Hay procesos pendientes de ejecución, espere a que alguno termine para iniciar un nuevo proceso!!!", false);
					}
				}
			}//if(runprocess == 'T'){
		}//if (cancel == 'T' && cancelold == 'F'){
	}//if(type == 'edit'){
}