/* ============================================================================================== */
	// Script name	  :  Facturacion Masiva | Scheduled
	// Script type	  :  Schedule
	// Script deploys : 
	// Description 	  : 
	// Author		  :  Agustín Leyva Vázquez- Netsoft
	// Lang 		  : Javascript
	// Date		      : 09/18/2013
	// Dependencias   : Campo customizado con id "custbody_massive_processid".
/* ============================================================================================== */
var CUSTOM_ERROR_CODE = 'Error_proceso_masivo_asiga_id';

function schedule_assign_recordid ()
{
	var MIN_REMAIN_USAGE = 4000;
	
	var numLine          = nlapiGetContext().getSetting('SCRIPT','custscript_numerolinea');
        numLine          = (numLine) ? parseInt(numLine) : 1; 
    var idToAssign       = nlapiGetContext().getSetting('SCRIPT','custscript_id_to_assign_gral');

	var recordtype = "customrecord_process_massivecfdi";
	var obj = nlapiLoadRecord(recordtype, idToAssign);
	var tipoTrans = obj.getFieldValue('custrecord_proc_cfdi_masiv_type');
	var xml       = obj.getFieldValue('custrecord_proc_cfdi_masiv_xml');
	var Folio     = '';
	if (xml != null && xml != ''){
		var wxml        = nlapiStringToXML(xml);
		var Comprobante = nlapiSelectNodes(wxml, "//*" );	
		Folio           = Comprobante[0].getAttribute('Folio');
		var Complemento = nlapiSelectNodes(wxml ,"//*[name()='tfd:TimbreFiscalDigital']" ); 
		var UUID        = Complemento[0].getAttribute('UUID');
	}
	
	switch(tipoTrans)
	{
		case "7": tipoTrans 	= 'invoice'; break;
		case "10": tipoTrans 	= 'creditmemo';break;
	}
	
	var tam_lista = obj.getLineItemCount("recmachcustrecord_dpfm_parent");
	var id_fact_actual;
	nlapiLogExecution('DEBUG', 'tam_lista: ', tam_lista);
	nlapiLogExecution('DEBUG', 'idToAssign: ', idToAssign);
	var metrica;
	for(var i = numLine ; i <= tam_lista; i++)
	{
		nlapiLogExecution('DEBUG', 'i: ', i);
		metrica = nlapiGetContext().getRemainingUsage();
		if((metrica < MIN_REMAIN_USAGE) && (i <= tam_lista))
		{							
			var params = new Array();				
				params['custscript_numerolinea']     = i;
				params['custscript_id_to_assign_gral']  = idToAssign;
				nlapiLogExecution('DEBUG', 'params: ', i);
			var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);				
			if(status == 'QUEUED')
			{
				nlapiLogExecution('DEBUG','Reencolado','Se termino la metrica del script: Posición i = ' + i);
				return;
			}
		}
		else
		{
			try	{
				
				id_fact_actual 	= obj.getLineItemValue("recmachcustrecord_dpfm_parent", "custrecord_dpfm_ids", i);
				//nlapiLogExecution('DEBUG', 'i: ', i);
				nlapiSubmitField(tipoTrans, id_fact_actual, "custbody_massive_processid", idToAssign);
				nlapiSubmitField(tipoTrans, id_fact_actual, "custbody_cfdixml", xml);
				nlapiSubmitField(tipoTrans, id_fact_actual, "custbody_foliosat", Folio);
				nlapiSubmitField(tipoTrans, id_fact_actual, "custbody_uuid", UUID);
			}
			catch (ex){
				nlapiLogExecution('ERROR', ex instanceof nlobjError ? ex.getCode() : CUSTOM_ERROR_CODE,
				ex instanceof nlobjError ? ex.getDetails() : 'JavaScript Error: ' + (ex.message != null ? ex.message : ex));
			}
		}
	}
	
}