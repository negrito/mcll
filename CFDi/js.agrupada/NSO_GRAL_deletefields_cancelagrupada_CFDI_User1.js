//===================================================================================================================================
// Script File	: NSO_GRAL_SetIDMAsiveProcess_CFDI_User.js
// Script Type  : User Event
// Description 	:
// Author		: Ivan Gonzalez G. - Netsoft
// Date			: 07-04-2016
//-----------------------------------------


function DeleteFielsCancel_AfterSubmit(type, form, request){

	if(type == 'edit'){

		var id        = nlapiGetRecordId();
		var record 	  = nlapiLoadRecord('customrecord_process_massivecfdi', id);
		var xml       = record.getFieldValue('custrecord_proc_cfdi_masiv_xml');
		var cancelado = record.getFieldValue('custrecord_proc_cfdi_masiv_cancel');	
		var wtype     = record.getFieldValue('custrecord_proc_cfdi_masiv_type');
		var wcancel   = record.getFieldValue('custrecord_proc_cfdi_masiv_nocanceled');

		nlapiLogExecution('DEBUG', 'xml: ', xml);
		nlapiLogExecution('DEBUG', 'cancelado: ', cancelado);
		nlapiLogExecution('DEBUG', 'wcancel: ', wcancel);
		
		if(xml != null && xml != '' && cancelado == 'T' && wcancel != 'T'){
			var params = new Array();				
			params['custscript_id_registro']  = id;
			
			var wstatus = nlapiScheduleScript('customscript_mass_cfdi_cancel_sched', 'customdeploy_mass_cfdi_cancel_sched', params);	
			if( wstatus == "QUEUED" ){
				nlapiSetRedirectURL("RECORD", "customrecord_process_massivecfdi", id, null, null);
			}															
			else{
				wstatus1 = nlapiScheduleScript('customscript_mass_cfdi_cancel_sched', 'customdeploy_mass_cfdi_cancel_sched_1', params);	
				if( wstatus1 == "QUEUED" ){
					nlapiSetRedirectURL("RECORD", "customrecord_process_massivecfdi", id, null, null);
				}															
				else{
					throw nlapiCreateError("ERROR", "Hay procesos pendientes de ejecución, espere a que alguno termine para iniciar un nuevo proceso!!!", false);
				}
			}
			
			//record.setFieldValue('custrecord_proc_cfdi_masiv_nocanceled', 'T');
			//nlapiSubmitRecord(record, true, true);
		}	
	}
}
