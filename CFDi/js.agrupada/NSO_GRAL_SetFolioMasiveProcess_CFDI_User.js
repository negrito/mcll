
// Script File	: NSO_GRAL_SetFolioMAsiveProcess_CFDI_User.js
// Script Type  : User Event
// Description 	:
// Author		: Ivan Gonzalez G. - Netsoft
// Date			: 24-08-2016
//-----------------------------------------

function SetFolio(type, form, request)
{
	if(type == 'new' || type == 'create' || type == 'edit'){
		var id        = nlapiGetRecordId();
		var record 	  = nlapiLoadRecord('customrecord_process_massivecfdi', id);
		var xml       = record.getFieldValue('custrecord_proc_cfdi_masiv_xml');
		if(xml != null && xml != ''){
			var wxml =  nlapiStringToXML(xml);	
			if (wxml != null && wxml != ''){
				var Comprobante = nlapiSelectNodes(wxml, "//*" );	
				var Folio = Comprobante[0].getAttribute('folio');
				if(Folio != null && Folio != ''){
					record.setFieldValue( "custrecord_proc_cfdi_masiv_foliosat", Folio);
					nlapiSubmitRecord(record, true, true);
				}
			}
		}
	}
}