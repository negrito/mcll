//===================================================================================================================================
// Script File	: Gral_Agrupada_ProcessElectronicInvoice.js
// Script Type  : Suitelet
// Description 	:
// Author		: Ivan Gonzalez G. - Netsoft
// Date			: 29-02-2016
// 				: Genera pantalla para facturación electrónica masiva.
//===================================================================================================================================

var idfieldCPS      = nlapiGetContext().getSetting('SCRIPT','custscript_id_campo_claveprod');
var idfieldUNM      = nlapiGetContext().getSetting('SCRIPT','custscript_id_campo_unidadmed');

var CUSTOM_ERROR_CODE = 'ERROR_CFDI_AGRUPADO';
function ProcElectronicInvoiceMassive(p_dateini, p_dateend, p_type, p_idtran, p_entity, p_subsidiary, p_location, p_currency, p_metpago, p_usodcfdi, p_FormaPago)
{	
	nlapiLogExecution('DEBUG', 'p_entity: ', p_entity);
	var params      = nlapiLoadRecord("customrecord_setup_cfdi", 1);
	var taxexento   = isNull(params.getFieldValue('custrecord_cfdi_taxexento'));
	var taxcodecero	= isNull(params.getFieldValue('custrecord_cfdi_taxcero'));
	var taxcode11 	= isNull(params.getFieldValue('custrecord_cfdi_tax11'));
	var taxcode16	= isNull(params.getFieldValue('custrecord_cfdi_tax16'));
	var vitem       = isNull(params.getFieldValue('custrecord_cfdi_masive_item'));
	var customer    = isNull(params.getFieldValue('custrecord_cfdi_customer_entity'));
	var ItemIEPS    = isNull(params.getFieldValue('custrecord_cfdi_idieps'))||'';
	var vitemDesc   = isNull(params.getFieldText('custrecord_cfdi_masive_item'));
	var Description = '';
	var transactions = new Array();
	var wtype       = (p_type == 'CustInvc') ? 7 : 10; 
	//-------> Valores de articulo y cliente para filtros seleccionados
	var searchresults = '';
	var idcustomer    = '';
	var iditemcfdi    = '';
	var ClaveUnidMed  = UnidadMedida()	
	if (p_entity == null || p_entity == ''){
		
		nlapiLogExecution('DEBUG', 'p_subsidiary: ', p_subsidiary);
		var filters = new Array();
		if (p_location != null && p_location != ''){
			filters.push ( new nlobjSearchFilter("custrecord_cfdi_entity_vs_location", null, "anyof", p_location)); 
		} 
		if (p_subsidiary != null && p_subsidiary != ''){
			filters.push ( new nlobjSearchFilter("custrecord_cfdi_entity_vs_subsidiary", null, "is", p_subsidiary));  
		}
		/*if (p_entity != null && p_entity != ''){
			filters.push ( new nlobjSearchFilter("custrecord_cfdi_entity_vs_entity", null, "anyof", p_entity)); 
		}*/  
		try	{
			var columns  = new Array();	
			columns.push ( new nlobjSearchColumn('custrecord_cfdi_entity_vs_entity'));
			columns.push ( new nlobjSearchColumn('custrecord_cfdi_entity_vs_item'));
			
			searchresults = nlapiSearchRecord('customrecord_cfdi_entity_vs_location', null, filters , columns);
			for (var i = 0;  searchresults != null && i < searchresults.length; i++){
				entity    = searchresults[i].getValue('custrecord_cfdi_entity_vs_entity');
				vitem     = searchresults[i].getValue('custrecord_cfdi_entity_vs_item');
				nlapiLogExecution('DEBUG', 'entity: ', entity);
				nlapiLogExecution('DEBUG', 'vitem: ', vitem);
				if(entity != null && entity != '' && vitem != null && vitem != ''){break;}
			}
		}
		catch (ex){
			nlapiLogExecution('ERROR', ex instanceof nlobjError ? ex.getCode() : CUSTOM_ERROR_CODE,
			ex instanceof nlobjError ? ex.getDetails() : 'JavaScript Error: ' + (ex.message != null ? ex.message : ex));
		}
	}//end  if (p_entity != null && p_entity != ''){
		
	else{
		//var entity  = isNull(params.getFieldValue('custrecord_cfdi_customer_entity'));
		var vitem   = isNull(params.getFieldValue('custrecord_cfdi_masive_item'));
	}
	
	try	{
	//--> Registry Process		
	
		//lee datos del item
		if (vitem != ''){
			var recitem = nsoGetItemRecord(vitem);
			var ClaveProdServ  = recitem.getFieldValue(idfieldCPS);
			var ClaveUnidad    = recitem.getFieldValue(idfieldUNM)||'';
			var UnidadAduana   = '';
			for (var c = 0; ClaveUnidMed  != '' && c< ClaveUnidMed.length; c++ ){
				var units = ClaveUnidMed[c].getValue('custrecord_cfdi_nat_units');
				if (units == ClaveUnidad){
					UnidadAduana = ClaveUnidMed[c].getValue('custrecord_cfdi_clav_unidad');
				}
			}
		}
		
		var newrecord    = nlapiCreateRecord('customrecord_process_massivecfdi');
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_start', nlapiDateToString(new Date(), "datetime"));
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_type', wtype);
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_status', 2);
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_subsidiaary', p_subsidiary);
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_location', p_location);
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_currency', p_currency);
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_metpago', p_metpago);
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_fpago', p_FormaPago);
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_usocfdi', p_usodcfdi);
		if (entity != null && entity != ''){
		newrecord.setFieldValue('custrecord_proc_cfdi_masiv_customer', entity);
		}
		else{
			if(p_entity != null && p_entity != ''){
				newrecord.setFieldValue('custrecord_proc_cfdi_masiv_customer', p_entity);
			}
			else{
				newrecord.setFieldValue('custrecord_proc_cfdi_masiv_customer', customer);
			}
		}
		
		
		if(p_idtran != null && p_idtran != '') {
			var xtype    = (p_type == 'CustInvc') ? 'invoice' : 'creditmemo'; 
			var record   = nlapiLoadRecord(xtype, p_idtran);
			var trandate = record.getFieldValue('trandate');
			var entity   = record.getFieldValue('entity');	
			if (entity != null && entity != ''){
			newrecord.setFieldValue('custrecord_proc_cfdi_masiv_customer', entity);}
			else
			{newrecord.setFieldValue('custrecord_proc_cfdi_masiv_customer', '');}
				
			newrecord.setFieldValue('custrecord_proc_cfdi_masiv_date', trandate);
			newrecord.selectNewLineItem('recmachcustrecord_dpfm_parent'); 
			newrecord.setCurrentLineItemValue('recmachcustrecord_dpfm_parent', 'custrecord_dpfm_ids', p_idtran);
			newrecord.commitLineItem('recmachcustrecord_dpfm_parent');	
			transactions.push(p_idtran);	
			   
			var j=1;
			while (j <= record.getLineItemCount('item')){
				var witemid   = record.getLineItemValue('item', 'item', j);
				if (witemid != ''){
					var recitem = nsoGetItemRecord(witemid);
					var ClaveProdServ  = recitem.getFieldValue(idfieldCPS);
					var ClaveUnidad    = recitem.getFieldValue(idfieldUNM);
				}
				var wdescitem = (record.getLineItemValue('item', 'description', j));
				var wquantity = Math.abs(parseFloat(record.getLineItemValue('item', 'quantity', j))); 
				var wrate     = Math.abs(parseFloat(record.getLineItemValue('item', 'rate', j))); 
				var wamount   = Math.abs(parseFloat(record.getLineItemValue('item', 'amount', j))); 
				var taxcode   = parseFloat(record.getLineItemValue('item', 'taxcode', j)); 
				var wtaxamt   = Math.abs( parseFloat(record.getLineItemValue('item', 'tax1amt', j)));
				var wtaxrate  = parseFloat(record.getLineItemValue('item', 'taxrate1', j));
				
				newrecord.selectNewLineItem('recmachcustrecord_dfm_fieldparent'); 
				newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_item', witemid);
				newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_description', wdescitem);
				newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_quantity', wquantity);
				newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_rate', wrate);
				newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_amount', wamount );
				newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_taxcode', taxcode);
				newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_tax', wtaxamt);
				newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_claveprodserv', ClaveProdServ);
				newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_clave_unid_med', ClaveUnidad);
				newrecord.commitLineItem('recmachcustrecord_dfm_fieldparent');	
				j++;
			}//while (j <= record.getLineItemCount('item')){
		}
		else{
			var wcount      = request.getLineItemCount("custpage_invoice_list");
			for(var  i = 1; i <= wcount; i++){
				var wmarked = request.getLineItemValue("custpage_invoice_list", "custpage_marked", i);
				
				if(wmarked == "T"){
					var wid        = request.getLineItemValue("custpage_invoice_list", "custpage_invoice", i);
					var total     = request.getLineItemValue("custpage_invoice_list", "custpage_total", i);
					
					newrecord.setFieldValue('custrecord_peim_date', '');
					newrecord.selectNewLineItem('recmachcustrecord_dpfm_parent'); 
					newrecord.setCurrentLineItemValue('recmachcustrecord_dpfm_parent', 'custrecord_dpfm_ids', wid);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dpfm_parent', 'custrecord_dpfm_total', total);
					newrecord.commitLineItem('recmachcustrecord_dpfm_parent');	
					transactions.push(wid);	
				}
			}//for(var  i = 1; i <= wcount; i++){
			
			//------> Agrega articulos a registro
			
			var searchresults = SearchInvoicesGroupTax_SinIeps(transactions, p_type, ItemIEPS)||'';
			nlapiLogExecution('DEBUG', 'searchresults: ', searchresults.length);
			for (var i = 0; searchresults != '' && i < searchresults.length; i++){
				var taxcode     = (searchresults[i].getValue('taxitem', null, 'group'));
				var taxcodeval  = nsoParseFloatOrZero(searchresults[i].getText('taxitem', null, 'group'));
				var fxamount    = Math.abs(nsoParseFloatOrZero(searchresults[i].getValue('fxamount',null, 'sum'))).toFixed(2);
				var taxamount   = Math.abs(nsoParseFloatOrZero(searchresults[i].getValue('taxamount',null, 'sum'))).toFixed(2);
				var amount      = Math.abs(nsoParseFloatOrZero(searchresults[i].getValue('amount',null, 'sum'))).toFixed(2);
				var tipocambio  = nsoParseFloatOrZero(fxamount/amount).toFixed(4);
					
				var grossamount = Math.abs(nsoParseFloatOrZero(fxamount) + nsoParseFloatOrZero(taxamount));
				var taxcodeid   = searchresults[i].getValue('taxitem', null, 'group');
				var wtaxrate    = '';
				//nlapiLogExecution('DEBUG', 'taxcodeid: ', taxcodeid);
				//nlapiLogExecution('DEBUG', 'vitem: ', vitem);
				if (vitemDesc == null || vitemDesc == ''){
					vitemDesc = 'Facturacion Masiva';
				}
				
				if (taxcode  == taxcode16)
				{wtaxrate = 16.00;}
				if (taxcode  == taxcode11)
				{wtaxrate = 11.00;}
				if (taxcode  == taxcodecero)
				{wtaxrate = 0.00;}
				
				taxamount   = nsoParseFloatOrZero(fxamount*(wtaxrate/100)).toFixed(2);
				
				if (taxcodeid == taxcode16 || taxcodeid == taxexento || taxcodeid == taxcode11 || taxcodeid == taxcodecero){
					newrecord.selectNewLineItem('recmachcustrecord_dfm_fieldparent'); 
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_item', vitem);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_description', vitemDesc);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_quantity', 1);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_rate', fxamount);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_amount', fxamount );
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_taxcode', wtaxrate);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_taxcodeid', taxcodeid);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_tax', taxamount);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_grossamount', grossamount);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_claveprodserv', ClaveProdServ);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_clave_unid_med', UnidadAduana);
					newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_porcentaje_ieps', '');
					newrecord.commitLineItem('recmachcustrecord_dfm_fieldparent');	
				}
			}// end for (var i = 0; i < searchresults.length; i++){
			
			if (ItemIEPS != ''){
			//------> Agrega articulos a registro Con IEPS
				var p_searchresults = SearchInvoicesGroupTaxIeps(transactions, p_type, ItemIEPS)||'';
				nlapiLogExecution('DEBUG', 'p_searchresults: ', p_searchresults.length);
				
				for (var i = 0; p_searchresults != '' &&  i < p_searchresults.length; i++){
					var taxcode     = (p_searchresults[i].getValue('taxitem', null, 'group'));
					var taxcodeval  = nsoParseFloatOrZero(p_searchresults[i].getText('taxitem', null, 'group'));
					var fxamount    = Math.abs(nsoParseFloatOrZero(p_searchresults[i].getValue('fxamount',null, 'sum'))).toFixed(2);
					var taxamount   = Math.abs(nsoParseFloatOrZero(p_searchresults[i].getValue('taxamount',null, 'sum'))).toFixed(2);
					var amount      = Math.abs(nsoParseFloatOrZero(p_searchresults[i].getValue('amount',null, 'sum'))).toFixed(2);
					var tipocambio  = nsoParseFloatOrZero(fxamount/amount).toFixed(4);
						taxamount   = nsoParseFloatOrZero(taxamount*tipocambio).toFixed(2);
					var grossamount = Math.abs(nsoParseFloatOrZero(fxamount) + nsoParseFloatOrZero(taxamount));
					var taxcodeid   = p_searchresults[i].getValue('taxitem', null, 'group');
					//var percieps    = nsoParseFloatOrZero(p_searchresults[i].getValue('custcol_ieps_applied', null, 'group'));
					var percieps    = nsoParseFloatOrZero(p_searchresults[i].getValue('custcol_cfdi_montosin_ieps', null, 'group'));
					var wtaxrate    = '';
					
					if (taxcode  == taxcode16)
					{wtaxrate = 16.00;}
					if (taxcode  == taxcode11)
					{wtaxrate = 11.00;}
					if (taxcode  == taxcodecero)
					{wtaxrate = 0.00;}
					
					if (taxcodeid == taxcode16 || taxcodeid == taxexento || taxcodeid == taxcode11 || taxcodeid == taxcodecero){
						newrecord.selectNewLineItem('recmachcustrecord_dfm_fieldparent'); 
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_item', vitem);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_description', vitemDesc);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_quantity', 1);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_rate', fxamount);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_amount', fxamount );
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_taxcode', wtaxrate);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_taxcodeid', taxcodeid);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_tax', taxamount);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_grossamount', grossamount);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_porcentaje_ieps', percieps);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_claveprodserv', ClaveProdServ);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_clave_unid_med', ClaveUnidad);
						newrecord.commitLineItem('recmachcustrecord_dfm_fieldparent');	
					}
				
				}// for (var i = 0; i < searchresults.length; i++){

				var SearchIEPS = SearchInvoicesIEPS(transactions, p_type, ItemIEPS)||'';
				nlapiLogExecution('DEBUG', 'SearchIEPS: ', SearchIEPS.length);
				
				for (var e = 0; SearchIEPS != '' && e < SearchIEPS.length; e++){
					
					var taxcodeieps     = (SearchIEPS[e].getValue('taxitem', null, 'group'));
					var perconteajeips  = nsoParseFloatOrZero(SearchIEPS[e].getValue('custcol_cfdi_montosin_ieps', null, 'group'));
					if (taxcodeieps == taxcode16 || taxcodeieps == taxexento || taxcodeieps == taxcode11 || taxcodeieps == taxcodecero){
					//if (taxcode == taxcodeieps && perconteajeips == percieps ){
						var taxcodevalieps  = nsoParseFloatOrZero(SearchIEPS[e].getText('taxitem', null, 'group'));
						var fxamountieps    = Math.abs(nsoParseFloatOrZero(SearchIEPS[e].getValue('fxamount',null, 'sum'))).toFixed(2);
						var taxamountieps   = Math.abs(nsoParseFloatOrZero(SearchIEPS[e].getValue('taxamount',null, 'sum'))).toFixed(2);
						var amountieps      = Math.abs(nsoParseFloatOrZero(SearchIEPS[e].getValue('amount',null, 'sum'))).toFixed(2);
						var tipocambioieps  = nsoParseFloatOrZero(fxamountieps/amountieps).toFixed(4);
							taxamountieps   = nsoParseFloatOrZero(taxamountieps*tipocambio).toFixed(2);
						var grossamountieps = Math.abs(nsoParseFloatOrZero(fxamountieps) + nsoParseFloatOrZero(taxamountieps));
						var taxcodeidieps   = SearchIEPS[e].getValue('taxitem', null, 'group');
						var wtaxrateIEPS    = '';
						if (taxcodeieps  == taxcode16)
						{wtaxrateIEPS = 16.00;}
						if (taxcodeieps  == taxcode11)
						{wtaxrateIEPS = 11.00;}
						if (taxcodeieps  == taxcodecero)
						{wtaxrateIEPS = 0.00;}
						
						newrecord.selectNewLineItem('recmachcustrecord_dfm_fieldparent'); 
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_item', ItemIEPS);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_description', 'IEPS');
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_quantity', 1);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_rate', amountieps);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_amount', amountieps );
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_taxcode', wtaxrateIEPS);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_taxcodeid', taxcodeidieps);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_tax', taxamountieps);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_grossamount', grossamountieps);
						newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_porcentaje_ieps', perconteajeips);
						newrecord.commitLineItem('recmachcustrecord_dfm_fieldparent');	
					}// if (taxcode == taxcodeieps){
				}//for (var e = 0; SearchIEPS != '' && e < SearchIEPS.length; e++){
			}//if (ItemIEPS != ''){
			
		}//end else
		
		//gift
		var amountGift = (nsoParseFloatOrZero(SearchInvoicesGift(transactions, p_type)));
		if(amountGift != 0){
			
			taxcode        = 0.00;
			taxcodeid      = taxcodecero;
			var newamount  = (nsoParseFloatOrZero(amountGift));
			taxamount      = 0;
			
			
			newrecord.selectNewLineItem('recmachcustrecord_dfm_fieldparent'); 
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_item', vitem);
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_description', 'Gift Certificate');
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_quantity', 1);
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_rate', newamount);
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_amount', newamount );
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_taxcode', taxcode);
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_taxcodeid', taxcodeid);
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_tax', taxamount);
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_grossamount', amountGift);
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_claveprodserv', ClaveProdServ);
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_clave_unid_med', ClaveUnidad);
			newrecord.setCurrentLineItemValue('recmachcustrecord_dfm_fieldparent', 'custrecord_dfm_porcentaje_ieps', '');
			newrecord.commitLineItem('recmachcustrecord_dfm_fieldparent');	
		}
		
		var wprocessid = nlapiSubmitRecord(newrecord, true);
		return wprocessid;
	}
	catch (ex){
		nlapiLogExecution('ERROR', ex instanceof nlobjError ? ex.getCode() : CUSTOM_ERROR_CODE,
		ex instanceof nlobjError ? ex.getDetails() : 'JavaScript Error: ' + (ex.message != null ? ex.message : ex));
	}
}
				

function SearchInvoicesGroupTax_SinIeps(p_transactions, p_type, ItemIEPS)
{				
	var searchresults = null;
	/*var params      = nlapiLoadRecord("customrecord_setup_cfdi", 1);
	var ItemIEPS    = isNull(params.getFieldValue('custrecord_cfdi_idieps'));*/
	if(p_type != null && p_type != ''){
				
		var filters = new Array();
	    filters.push ( new nlobjSearchFilter("type", null, "anyof", p_type)); 
        filters.push ( new nlobjSearchFilter("mainline", null, "is", 'F')); 
		filters.push ( new nlobjSearchFilter('taxline', null, 'is', 'F'));
		filters.push ( new nlobjSearchFilter('taxitem', null, 'noneof', "@NONE@"));	
		filters.push ( new nlobjSearchFilter("internalid", null, "anyof", p_transactions)); 
		filters.push ( new nlobjSearchFilter("closed", null, "anyof", 'F')); 
		if (ItemIEPS != ''){
			filters.push ( new nlobjSearchFilter("item", null, "noneof", ItemIEPS));
			filters.push ( new nlobjSearchFilter("custcol_ieps_applied", null, "is", 'F'));
		}
		var columns  = new Array();	
		columns.push ( new nlobjSearchColumn('fxamount',null, 'sum'));
		columns.push ( new nlobjSearchColumn("taxamount",null, 'sum'));
		columns.push ( new nlobjSearchColumn('taxitem', null, 'group'));
		columns.push ( new nlobjSearchColumn('amount', null, 'sum'));	
		searchresults = nlapiSearchRecord('transaction', null, filters , columns);
	}
	
	return searchresults;
}

function SearchInvoicesGroupTaxIeps(p_transactions, p_type, ItemIEPS)
{				
	var searchresults = null;
	/*var params      = nlapiLoadRecord("customrecord_setup_cfdi", 1);
	var ItemIEPS    = isNull(params.getFieldValue('custrecord_cfdi_idieps'));*/
	if(p_type != null && p_type != ''){
				
		var filters = new Array();
	    filters.push ( new nlobjSearchFilter("type", null, "anyof", p_type)); 
        filters.push ( new nlobjSearchFilter("mainline", null, "is", 'F')); 
		filters.push ( new nlobjSearchFilter('taxline', null, 'is', 'F'));
		filters.push ( new nlobjSearchFilter('taxitem', null, 'noneof', "@NONE@"));	
		filters.push ( new nlobjSearchFilter("internalid", null, "anyof", p_transactions)); 
		filters.push ( new nlobjSearchFilter("closed", null, "anyof", 'F')); 
		filters.push ( new nlobjSearchFilter("item", null, "noneof", ItemIEPS));
		filters.push ( new nlobjSearchFilter("custcol_ieps_applied", null, "is", 'T'));
			
		var columns  = new Array();	
		columns.push ( new nlobjSearchColumn('fxamount',null, 'sum'));
		columns.push ( new nlobjSearchColumn("taxamount",null, 'sum'));
		columns.push ( new nlobjSearchColumn('taxitem', null, 'group'));
		columns.push ( new nlobjSearchColumn('amount', null, 'sum'));	
		columns.push ( new nlobjSearchColumn('custcol_cfdi_montosin_ieps', null, 'group'));	
		searchresults = nlapiSearchRecord('transaction', null, filters , columns);
	}
	
	return searchresults;
}				




function SearchInvoicesIEPS(p_transactions, p_type, ItemIEPS)
{
	var searchresults = 0;
	/*var params      = nlapiLoadRecord("customrecord_setup_cfdi", 1);
	var ItemIEPS    = isNull(params.getFieldValue('custrecord_cfdi_idieps'));*/
	if(p_transactions != null && p_transactions != ''){
				
		var filters = new Array();
	    filters.push ( new nlobjSearchFilter("type", null, "anyof", p_type)); 
		filters.push ( new nlobjSearchFilter("mainline", null, "is", 'F')); 
		filters.push ( new nlobjSearchFilter('taxline', null, 'is', 'F'));
		filters.push ( new nlobjSearchFilter('taxitem', null, 'noneof', "@NONE@"));	
        filters.push ( new nlobjSearchFilter("internalid", null, "anyof", p_transactions)); 
		filters.push ( new nlobjSearchFilter("closed", null, "anyof", 'F')); 
		filters.push ( new nlobjSearchFilter("item", null, "anyof", ItemIEPS));
			
		var columns  = new Array();	
		columns.push ( new nlobjSearchColumn('fxamount',null, 'sum'));
		columns.push ( new nlobjSearchColumn("taxamount",null, 'sum'));
		columns.push ( new nlobjSearchColumn('taxitem', null, 'group'));
		columns.push ( new nlobjSearchColumn('amount', null, 'sum'));	
		columns.push ( new nlobjSearchColumn('custcol_cfdi_montosin_ieps', null, 'group'));
		searchresults = nlapiSearchRecord('transaction', null, filters , columns);
	}
	//nlapiLogExecution('DEBUG', 'searchresults: ', searchresults);
	return searchresults;
	
}			


function nsoParseFloatOrZero(f)
{
   var r=parseFloat(f);
   return isNaN(r) ? 0 : r;
}

function nsoParseIntOrZero(i)
{
   var r=parseInt(i);
   return isNaN(r) ? 0 : r;
}


function isNull(value) {
    return (value == null) ? '' : value;
}

function SearchParamsLoc(location)
{
	var idcustomer = '';
	if (location != null && location != ''){
		var filters = new Array();
	    filters.push ( new nlobjSearchFilter("custrecord_cfdi_location_vs_entity", null, "anyof", location)); 
       		
		var columns  = new Array();	
		columns[0]   = new nlobjSearchColumn('custrecord_cfdi_entity_vs_location');
		
		searchresults = nlapiSearchRecord('customrecord_cfdi_entity_vs_location', null, filters , columns);
		for (var i = 0;  searchresults != null && i < searchresults.length; i++){
			var idcustomer     = searchresults[i].getValue('custrecord_cfdi_entity_vs_location');
			if(idcustomer != null && idcustomer != ''){break;}
		}
	}
	
	return idcustomer;
}

function SearchInvoicesGift(p_transactions, p_type)
{
	var amountgift = 0;
	
	if(p_transactions != null && p_transactions != ''){
				
		var filters = new Array();
	    filters.push ( new nlobjSearchFilter("type", null, "anyof", p_type)); 
        filters.push ( new nlobjSearchFilter("internalid", null, "anyof", p_transactions)); 
		filters.push (new nlobjSearchFilter("type", 'item', "anyof", 'GiftCert'));
			
		var columns  = new Array();	
		columns.push ( new nlobjSearchColumn('fxamount',null, 'sum'));
		
		searchresults = nlapiSearchRecord('transaction', null, filters , columns);
		for (var i = 0;  searchresults != null && i < searchresults.length; i++){
			amountgift     += parseFloat(searchresults[i].getValue('fxamount',null, 'sum'));
		}
	}
	
	return amountgift;
	
}

function SearchInvoicesDiscount(p_transactions, p_type)
{
	var amountdisc = 0;
	
	if(p_transactions != null && p_transactions != ''){
				
		var filters = new Array();
	    filters.push ( new nlobjSearchFilter("type", null, "anyof", p_type)); 
        filters.push ( new nlobjSearchFilter("internalid", null, "anyof", p_transactions)); 
		filters.push ( new nlobjSearchFilter("transactiondiscount", null, "is", 'T'));
	    filters.push ( new nlobjSearchFilter("mainline", null, "is", 'F')); 	
		var columns  = new Array();	
		columns.push ( new nlobjSearchColumn('fxamount',null, 'sum'));
		
		var searchresults = nlapiSearchRecord('transaction', null, filters , columns);
		for (var i = 0;  searchresults != null && i < searchresults.length; i++){
			amountdisc     += parseFloat(searchresults[i].getValue('fxamount',null, 'sum'));
		}
	}
	
	return amountdisc;
	
}

function nsoGetItemRecord(id)
{
	var itemrecord = null;

	var searchresults = nlapiSearchRecord('item', null, new nlobjSearchFilter('internalid', null, 'is', id));
	
	if (searchresults != null && searchresults.length > 0)
	{
		itemrecord = nlapiLoadRecord(searchresults[0].getRecordType(), searchresults[0].getId());
	}
	
	return itemrecord;
}


function UnidadMedida()
{
	var searchresult = '';
	
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_cfdi_clav_unidad'));
	columns.push(new nlobjSearchColumn('custrecord_cfdi_nat_units'));
	searchresult  = nlapiSearchRecord('customrecord_cfdi_clave_unidad', null, null, columns);

	return searchresult;
}


