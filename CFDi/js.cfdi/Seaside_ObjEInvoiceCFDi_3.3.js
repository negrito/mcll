//===========================================================================================
// Company        : NetSoft, www.netsoft.com.mx
// Name of Script : Seaside_ObjEInvoiceCFDi.js
// Author         : Ivan Gonzalez - Netsoft - Mexico
// Date			  : 22-02-2012
// Base			  : DEV
// Type           : Library
// Sub-Type       : Business Process
// Categories     : Business logic
// Description    : Generate eInvoice layout 
// NetSuite Ver.  : (Edition: International) Release 2011.2
// Dependences    : File Name
//                   --------------------
//                   NSOUtils.js
//                   NSOCountriesStates.js
//===========================================================================================


var idretIVA = 543535224352;
var idretISR = 461253455529;
//nlapiLogExecution('DEBUG','metrica: ' + metrica);
function NSObjEInvoiceCFDi() 
{
    
    this.invoice       = null;
    this.customer      = null;
    this.params        = null;
	this.setupcfdi     = null;
    this.salesorder    = null;
	this.body          = "";	
	this.subtotal      = 0;
	this.subtotalbruto = 0;
	this.tasades       = null;
	this.tasatax       = null;
	this.tasaretencion = null;		

    this.build = function(id, splitter, newline, setupid) 
    {
		splitter = splitter == null || splitter == "" ? "|" : splitter;
		newline  = newline == null || newline == "" ? "\n" : newline;
		setupid  = setupid == null || setupid == "" ? 1 : setupid;
	
        var invoice     = nsoGetTranRecord(id); 
        var custid      = invoice.getFieldValue("entity");
        var customer    = nlapiLoadRecord("customer", custid);
        var params      = nlapiLoadRecord("customrecord_cfdisetup", setupid);
		var setupcfdi   = nlapiLoadRecord("customrecord_setup_cfdi", 1);	
        
		  //----> Asignación de variables globales
		this.id          = id;
        this.invoice     = invoice;
        this.customer    = customer;
        this.params      = params;        
		this.setupid     = setupid;
		
		//----> Generación de XML
		var bodytext = '';
		bodytext += '<fx:FactDocMX xmlns:fx="http://www.fact.com.mx/schema/fx" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.fact.com.mx/schema/fx http://www.mysuitemex.com/fact/schema/fx_2010_d.xsd">';

        bodytext += '<fx:Version>5</fx:Version>';
      	bodytext += nlGetCFDi01(invoice, customer, params, setupcfdi);
		bodytext += nlGetCFDiH4(invoice, customer, params, setupcfdi);
        bodytext += nlGetCFDi02(invoice, customer, params, setupcfdi);
        bodytext += Totales(invoice, customer, params, setupcfdi);
        bodytext += ComprobanteEx(invoice, customer, params, setupcfdi);
		
        this.body = bodytext;
		bodytext = bodytext.replace(/á/g, "&#225;");
		bodytext = bodytext.replace(/é/g, "&#233;");
		bodytext = bodytext.replace(/í/g, "&#237;");
		bodytext = bodytext.replace(/ó/g, "&#243;");
		bodytext = bodytext.replace(/ú/g, "&#250;");
		bodytext = bodytext.replace(/Á/g, "&#193;");
		bodytext = bodytext.replace(/É/g, "&#201;");
		bodytext = bodytext.replace(/Í/g, "&#205;");
		bodytext = bodytext.replace(/Ó/g, "&#211;");
		bodytext = bodytext.replace(/Ú/g, "&#218;");
		bodytext = bodytext.replace(/Ò/g, "&#210;");
		bodytext = bodytext.replace(/ñ/g, "&#241;");
		bodytext = bodytext.replace(/Ñ/g, "&#209;");
		bodytext = bodytext.replace(/®/g, "&#174;");
		bodytext = bodytext.replace(/©/g, "&#169;");
		bodytext = bodytext.replace(/ª/g, "&#170;");
		bodytext = bodytext.replace(/™/g, "&#153;");
		bodytext = bodytext.replace(/Ÿ/g, "&#159;");
		bodytext = bodytext.replace(/°/g, "&#176;");
		bodytext = bodytext.replace(/¼/g, "&#188;");
		bodytext = bodytext.replace(/½/g, "&#189;");
		bodytext = bodytext.replace(/¾/g, "&#190;");
		bodytext = bodytext.replace(/Ä/g, "&#196;");
		bodytext = bodytext.replace(/Ë/g, "&#203;");
		bodytext = bodytext.replace(/Ï/g, "&#207;");
		bodytext = bodytext.replace(/Ö/g, "&#214;");
		bodytext = bodytext.replace(/Ü/g, "&#220;");
		bodytext = bodytext.replace(/ÿ/g, "&#255;");
        return bodytext;
    }
}




//-----------------------------------Inicia trama 01------------------------------------------------------------------------------------
 


function nlGetCFDi01(invoice, customer, params, setupcfdi)
{

    var folio = invoice.getFieldValue('tranid');
	var FolioFiscalOrigUuid = '';//632f736a-35a3-44f2-bae4-dd94e3ebdd53
	var SerieFolioFiscalOrig = '';
	var FechaFolioFiscalOrig = '';
	var MontoFolioFiscalOrig = '';
	var emailsend            = params.getFieldValue('custrecord_cfdi_emailenvio')
	var emailcustomer        = isNull(invoice.getFieldValue('custbody_email_cliente'));
	var sendemail            = setupcfdi.getFieldValue('custrecord_cfdi_mailalcliente');
	var emailtest            = setupcfdi.getFieldValue('custrecord_cfdi_email_send_test');
	var testing              = setupcfdi.getFieldValue('custrecord_cfdi_testing');
	var NumCtaPago           = invoice.getFieldValue('custbody_numctapago') 
	
	var retval = '';
	
	retval += '<fx:Identificacion>';
	retval += '<fx:CdgPaisEmisor>MX</fx:CdgPaisEmisor>';
	retval += '<fx:TipoDeComprobante>' + isNull(getTipoDocumento(invoice.getRecordType()))+ '</fx:TipoDeComprobante>';	
	if(testing == 'T')	{
	 	retval += '<fx:RFCEmisor>' + setupcfdi.getFieldValue('custrecord_cfdi_rfctesting') + '</fx:RFCEmisor>';	
	}
	else	{
		retval += '<fx:RFCEmisor>' + params.getFieldValue('custrecord_cfdi_rfcemisor') + '</fx:RFCEmisor>';	
	} 
	retval += '<fx:RazonSocialEmisor>' + isNull(params.getFieldValue('custrecord_cfdi_nombreemisor')) + '</fx:RazonSocialEmisor>';
	retval += '<fx:Usuario>' + params.getFieldValue('custrecord_cfdi_usuario') + '</fx:Usuario>';
	var tipodedocumento = getTipoDocumento(invoice.getRecordType());
	retval += '<fx:NumeroInterno>' + tipodedocumento + ': ' + folio + '</fx:NumeroInterno>';
	retval += '<fx:LugarExpedicion>'  + params.getFieldValue('custrecord_cfdi_mun_expedidoen') + ', ' +  params.getFieldValue('custrecord_cfdi_estadoemisor') + '</fx:LugarExpedicion>';	
	if (NumCtaPago != null && NumCtaPago != ''){		
		retval += '<fx:NumCtaPago>' + NumCtaPago + '</fx:NumCtaPago>';	
	}
	retval += '<fx:NumCtaPago>' + invoice.getFieldValue('custbody_cfdi_numctapago') + '</fx:NumCtaPago>';	
	
	if (FolioFiscalOrigUuid != null && FolioFiscalOrigUuid != '')
	{
		retval += '<fx:TotalDeParcialidades>';	
		retval += '<fx:FolioFiscalOrigUuid>'  + FolioFiscalOrigUuid  + '</fx:FolioFiscalOrigUuid>';
		if (SerieFolioFiscalOrig != null && SerieFolioFiscalOrig != '')
		{retval += '<fx:SerieFolioFiscalOrig>' + SerieFolioFiscalOrig + '</fx:SerieFolioFiscalOrig>';}
		retval += '<fx:FechaFolioFiscalOrig>' + FechaFolioFiscalOrig + '</fx:FechaFolioFiscalOrig>';
		retval += '<fx:MontoFolioFiscalOrig>' + MontoFolioFiscalOrig + '</fx:MontoFolioFiscalOrig>';
		retval += '</fx:TotalDeParcialidades>';	
	}
		
	retval += '</fx:Identificacion>';
	
	//------> Envio de correo a cuenta de pruebas
	if (testing == 'T' ){
		if(emailtest != null && emailtest != '') {
			retval += '<fx:Procesamiento>';
			retval += '<fx:Dictionary name="email">';
			retval += '<fx:Entry k="to" v="' +emailtest+'"/>';
			retval += '</fx:Dictionary>';
			retval += '</fx:Procesamiento>';
		}
	}
	else {
		//------> Envio de correo al cliente
		if (sendemail == 'T'){	
			if ((emailsend != null && emailsend != '')||(emailcustomer != null && emailcustomer != '')){
				retval += '<fx:Procesamiento>';
				retval += '<fx:Dictionary name="email">';
				if (emailcustomer != null && emailcustomer != '' && emailsend != null && emailsend != ""){
					retval += '<fx:Entry k="to" v="' + emailcustomer + ';'+ emailsend +'"/>';
				}
				else if((emailcustomer == null || emailcustomer == '') && emailsend != null && emailsend != ""){
					retval += '<fx:Entry k="to" v="' +emailsend+'"/>';
				}
				else if((emailsend == null || emailsend == '') && emailcustomer != null && emailcustomer != ""){
					retval += '<fx:Entry k="to" v="' + emailcustomer +'"/>';
				}
				retval += '</fx:Dictionary>';
				retval += '</fx:Procesamiento>';
			}
		}
		else{
			retval += '<fx:Procesamiento>';
			retval += '<fx:Dictionary name="email">';
			if (emailsend != null && emailsend != ""){
				retval += '<fx:Entry k="to" v="' + emailsend +'"/>';
			}		
			retval += '</fx:Dictionary>';
			retval += '</fx:Procesamiento>';
		}
	}
	
		
	//----> Datos Emisor	
	retval     += '<fx:Emisor>';	
	retval     += '<fx:DomicilioFiscal>';	
	var calle   = (params.getFieldValue('custrecord_cfdi_calleemisor'));	
	retval     += '<fx:Calle>' + calle + '</fx:Calle>'; 	   
	var numext = isNull(params.getFieldValue('custrecord_cfdi_numexterioremisor'));
	if (numext != null && numext != ''){
		retval += '<fx:NumeroExterior>' + numext + '</fx:NumeroExterior>'; 	    
	}	
	var numint = isNull(params.getFieldValue('custrecord_cfdi_numinterioremisor'));
	if (numint != null && numint != ''){																								
   	    retval += '<fx:NumeroInterior>' + numint + '</fx:NumeroInterior>'; 
	}	
	var localidad = isNull(params.getFieldValue('custrecord_cfdi_localidademisor'));
	if (localidad != null && localidad != ''){
    	retval += '<fx:Localidad>' + localidad + '</fx:Localidad>';    
	}	
	var referencia = isNull(params.getFieldValue('custrecord_cfdi_referenciaemisor'));
	if (referencia != null && referencia != ''){
		retval += '<fx:Referencia>' + referencia + '</fx:Referencia>';   
	}	
	var colonia = isNull(params.getFieldValue('custrecord_cfdi_coloniaemisor'));
	if (colonia != null && colonia != ''){
    	retval += '<fx:Colonia>' + colonia + '</fx:Colonia>'; 
	}	
	var municipio = isNull(params.getFieldValue('custrecord_cfdi_municipioemisor'));
	if (municipio != null && municipio != ''){
    	retval += '<fx:Municipio>'  + municipio + '</fx:Municipio>';   
	}
	
    retval += '<fx:Estado>' + isNull(params.getFieldValue('custrecord_cfdi_estadoemisor')) + '</fx:Estado>';	    
    retval += '<fx:Pais>'  + isNull(params.getFieldValue('custrecord_cfdi_paisemisor')) + '</fx:Pais>'; 	    
    retval += '<fx:CodigoPostal>'  + isNull(params.getFieldValue('custrecord_cfdi_cpemisor')) + '</fx:CodigoPostal>';  	
	
	var Telefono = isNull(params.getFieldValue('custrecord_cfdi_telcorporativo'));
	if (Telefono != null && Telefono != ''){
		retval += '<fx:TelContacto>'  + Telefono+ '</fx:TelContacto>';  	
	}
				
	retval += '</fx:DomicilioFiscal>';	
	
	//------------->Empiezan datos del lugar de emision <--------------------------------// 
	
	var estadoemision = isNull(params.getFieldValue('custrecord_cfdi_estado_expedidoen'));
	var paisemision = isNull(params.getFieldValue('custrecord_cfdi_pais_expedidoen'));
	var CPEmision = isNull(params.getFieldValue('custrecord_cfdi_cp_expedidoen'));
	
	if( (estadoemision != null && estadoemision != '') && (paisemision != null && paisemision != '') && (CPEmision != null && CPEmision != '') )
	{
		retval += '<fx:DomicilioDeEmision>';
			
		var calleemi = isNull(params.getFieldValue('custrecord_cfdi_calleexpedidoen'));
		if (calleemi != null && calleemi != '')
		{
			retval += '<fx:Calle>' + calleemi + '</fx:Calle>'; 	   
		}	
		var numextemi = isNull(params.getFieldValue('custrecord_cfdi_numext_expedidoen'));
		if (numextemi != null && numextemi != '')
		{
			retval += '<fx:NumeroExterior>' + numextemi + '</fx:NumeroExterior>'; 	    
		}	
		var numintemi = isNull(params.getFieldValue('custrecord_cfdi_numint_expedidoen'));
		if (numintemi != null && numintemi != '')
		{																								
			retval += '<fx:NumeroInterior>' + numintemi + '</fx:NumeroInterior>'; 
		}	
		var localidademi = isNull(params.getFieldValue('custrecord_cdfi_loc_expedidoen'));
		if (localidademi != null && localidademi != '')
		{
			retval += '<fx:Localidad>' + localidademi + '</fx:Localidad>';    
		}	
		var referenciaemi = isNull(params.getFieldValue('custrecord_cfdi_ref_expedidoen'));
		if (referenciaemi != null && referenciaemi != '')
		{
			retval += '<fx:Referencia>' + referenciaemi + '</fx:Referencia>';   
		}	
		var coloniaemi = isNull(params.getFieldValue('custrecord_cfdi_colonia_expedidoen'));
		if (coloniaemi != null && coloniaemi != '')
		{
			retval += '<fx:Colonia>' + coloniaemi + '</fx:Colonia>'; 
		}
		var municipioemi = isNull(params.getFieldValue('custrecord_cfdi_mun_expedidoen'));
		if (municipioemi != null && municipioemi != '')
		{
			retval += '<fx:Municipio>'  + municipioemi + '</fx:Municipio>';   
		}
		
		retval += '<fx:Estado>' + estadoemision + '</fx:Estado>';	    
		retval += '<fx:Pais>'  + paisemision + '</fx:Pais>'; 	    
		retval += '<fx:CodigoPostal>' + CPEmision + '</fx:CodigoPostal>';  		
		retval += '</fx:DomicilioDeEmision>';
	}
		
	retval += '<fx:RegimenFiscal>';
	retval += '<fx:Regimen>' + isNull(params.getFieldValue('custrecord_cfdi_regimenfiscal')) + '</fx:Regimen>'
	retval += '</fx:RegimenFiscal>';	    	
	retval += '</fx:Emisor>';	
	retval  = retval.replace(/&/g, "&amp;");
	
	
    return retval;
}

function nlGetCFDiH4(invoice, customer, params, setupcfdi)
{
  	var retval = '';
    var billaddresslist = invoice.getFieldValue("billaddresslist");
	var shipaddresslist = isNull(invoice.getFieldValue("shipaddresslist"));
	var recRFC = invoice.getFieldValue('custbody_rfc');
	if ( recRFC != null && recRFC != '')
	{
		recRFC = recRFC.replace(/-/g, "");
		recRFC = recRFC.replace(/\s/g, "");
		
	}
	var nombreenvio = isNull(customer.getFieldValue('companyname'));
	var retvalreceptor = '';
	var retvalDomRec = '';
	var nombreenvio = isNull(customer.getFieldValue('companyname'));
	retval += '<fx:Receptor>';
	
  	if (billaddresslist == null || billaddresslist == '')  
	{
		//retval = '<fx:Receptor>';		
		var calleenvio     = isNull(invoice.getFieldValue('billaddr1'));
		var coloniaenvio   = isNull(invoice.getFieldValue('billaddr2'));
		var municipioenvio = isNull(invoice.getFieldValue('billcity'));
		var estadoenvio    = isNull(invoice.getFieldValue('billstate'));
		var cpenvio        = isNull(invoice.getFieldValue('billzip'));
		var codigopais     = isNull(invoice.getFieldValue('billcountry'));
		var pais           = nsoGetCountryName(invoice.getFieldValue('billcountry'));
		
		retval += '<fx:CdgPaisReceptor>'+ codigopais +'</fx:CdgPaisReceptor>';
		retval += '<fx:RFCReceptor>' + recRFC + '</fx:RFCReceptor>';  		
		if (nombreenvio != null && nombreenvio != '')
		{retval += '<fx:NombreReceptor>' + nombreenvio + '</fx:NombreReceptor>';}	
		
		
		if ((calleenvio != null && calleenvio != '')||(municipioenvio != null && municipioenvio != '') || (estadoenvio != null && estadoenvio != '') ||(pais != null && pais != '') || (cpenvio != null && cpenvio != '') ){
			
			retvalreceptor += '<fx:Domicilio>';
			if (codigopais != 'MX')	{retvalreceptor += '<fx:OtroDomicilio>';}
			else
			{retvalreceptor += '<fx:DomicilioFiscalMexicano>';}
			if (calleenvio != null && calleenvio != ''){
				retvalreceptor += '<fx:Calle>' + isNull(calleenvio) + '</fx:Calle>'; } 
			if (coloniaenvio != null && coloniaenvio != ''){
				retvalreceptor += '<fx:Colonia>' + coloniaenvio + '</fx:Colonia>';}
			if (municipioenvio != null && municipioenvio != ''){	
				retvalreceptor += '<fx:Municipio>' + municipioenvio + '</fx:Municipio>';  }
			if (estadoenvio != null && estadoenvio != ''){
			retvalreceptor += '<fx:Estado>' + estadoenvio + '</fx:Estado>';  }
			if (pais != null && pais != ''){
				retvalreceptor += '<fx:Pais>'+ pais +'</fx:Pais>';  }
			if (cpenvio != null && cpenvio != ''){ 
				retvalreceptor += '<fx:CodigoPostal>' + cpenvio + '</fx:CodigoPostal>';   }
			if (codigopais != 'MX')	{
				retvalreceptor += '</fx:OtroDomicilio>';}
			else{
				retvalreceptor += '</fx:DomicilioFiscalMexicano>';
			}
			retvalreceptor += '</fx:Domicilio>';
		}
		retval += retvalreceptor;
		//retval += '</fx:Receptor>';
		
	}
 	else{
		
		for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++){
			
			if (customer.getLineItemValue("addressbook", "id", i) == billaddresslist){
				
				var codigopais    = (customer.getLineItemValue('addressbook', 'country', i));
				var pais          = nsoGetCountryName(customer.getLineItemValue('addressbook', 'country', i));
				var callereceptor = isNull(customer.getLineItemValue('addressbook', 'addr1', i));
				var colonia       = isNull(customer.getLineItemValue('addressbook', 'addr2', i));
				var Municipio     = isNull(customer.getLineItemValue('addressbook', 'city', i));
				var Estado        = isNull(customer.getLineItemValue('addressbook', 'state', i));
				var CP            = isNull(customer.getLineItemValue('addressbook', 'zip', i));								
				
				if (codigopais != null && codigopais != ''){
					retval += '<fx:CdgPaisReceptor>' + codigopais + '</fx:CdgPaisReceptor>';
					retval += '<fx:RFCReceptor>' + recRFC + '</fx:RFCReceptor>'; 
					if (nombreenvio != null && nombreenvio != '')
					{retval += '<fx:NombreReceptor>' + nombreenvio + '</fx:NombreReceptor>';}
				}
				if ((callereceptor != null && callereceptor != '')||(Municipio != null && Municipio != '') || (Estado != null && Estado != '') ||(pais != null && pais != '') || (CP != null && CP != '') ){
					retvalreceptor += '<fx:Domicilio>';
					
					if (codigopais != 'MX')	
					{retvalreceptor += '<fx:OtroDomicilio>';}
					else
					{retvalreceptor += '<fx:DomicilioFiscalMexicano>';}
					if (callereceptor != null && callereceptor != '')
					{retvalreceptor += '<fx:Calle>' + callereceptor + '</fx:Calle>';}   
					if (colonia != null && colonia != '')
					{retvalreceptor += '<fx:Colonia>' + colonia + '</fx:Colonia>';}
					if (Municipio != null && Municipio !='')
					{retvalreceptor += '<fx:Municipio>' + Municipio + '</fx:Municipio>';}
					if (Estado != null && Estado != '')
					{retvalreceptor += '<fx:Estado>' + Estado + '</fx:Estado>';}
					if (pais != null && pais != '')
					retvalreceptor += '<fx:Pais>' + pais + '</fx:Pais>';           
					if (CP != null && CP != '')
					{retvalreceptor += '<fx:CodigoPostal>' + CP + '</fx:CodigoPostal>';} 
						
					if (codigopais != 'MX')	
					{retvalreceptor += '</fx:OtroDomicilio>';}
					else
					{retvalreceptor += '</fx:DomicilioFiscalMexicano>';}
					
					retvalreceptor += '</fx:Domicilio>';
					nlapiLogExecution('DEBUG','retvalreceptor ', retvalreceptor); 
				}
			}//end 
		}// end for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++) 
		for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++){	
			
			if (customer.getLineItemValue("addressbook", "id", i) == shipaddresslist)
			{
				var codigopaisenvio    = (customer.getLineItemValue('addressbook', 'country', i));
				var paisenvio          = nsoGetCountryName(customer.getLineItemValue('addressbook', 'country', i));
				var callereceptorenvio = isNull(customer.getLineItemValue('addressbook', 'addr1', i));
				var coloniaenvio       = isNull(customer.getLineItemValue('addressbook', 'addr2', i));
				var Municipioenvio     = isNull(customer.getLineItemValue('addressbook', 'city', i));
				var Estadoenvio        = isNull(customer.getLineItemValue('addressbook', 'state', i));
				var CPenvio            = isNull(customer.getLineItemValue('addressbook', 'zip', i));	
				var retvalenvio        = '';
				
				if (retval == null || retval == ''){
					retval += '<fx:CdgPaisReceptor>' + codigopaisenvio + '</fx:CdgPaisReceptor>';
					retval += '<fx:RFCReceptor>' + recRFC + '</fx:RFCReceptor>'; 
					if (nombreenvio != null && nombreenvio != '')
					{retval += '<fx:NombreReceptor>' + nombreenvio + '</fx:NombreReceptor>';}

				}

				retvalDomRec += '<fx:DomicilioDeRecepcion>';
			
				if (codigopaisenvio != 'MX')	
				{retvalDomRec += '<fx:OtroDomicilio>';}
				else
				{retvalDomRec += '<fx:DomicilioFiscalMexicano>';}
				if (callereceptorenvio != null && callereceptorenvio != '')
				{retvalDomRec += '<fx:Calle>' + callereceptorenvio + '</fx:Calle>';}   
				if (coloniaenvio != null && coloniaenvio != '')
				{retvalDomRec += '<fx:Colonia>' + coloniaenvio + '</fx:Colonia>';}
				if (Municipioenvio != null && Municipioenvio !='')
				{retvalDomRec += '<fx:Municipio>' + Municipioenvio + '</fx:Municipio>';}
				if (Estadoenvio != null && Estadoenvio != '')
				{retvalDomRec += '<fx:Estado>' + Estadoenvio + '</fx:Estado>';}
				if (paisenvio != null && paisenvio != '')
				retvalDomRec += '<fx:Pais>' + paisenvio + '</fx:Pais>';           
				if (CPenvio != null && CPenvio != '')
				{retvalDomRec += '<fx:CodigoPostal>' + CPenvio + '</fx:CodigoPostal>';} 
					
				if (codigopaisenvio != 'MX')	
				{retvalDomRec += '</fx:OtroDomicilio>';}
				else
				{retvalDomRec += '</fx:DomicilioFiscalMexicano>';}
				
				retvalDomRec += '</fx:DomicilioDeRecepcion>';
				nlapiLogExecution('DEBUG','retvalDomRec ', retvalDomRec); 
				//break;
			}// end if (customer.getLineItemValue("addressbook", "id", i) == shipaddresslist)
		}// end for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++) 
		
	}// end else
	if ((retvalreceptor != null && retvalreceptor != '')||(retvalDomRec != null && retvalDomRec != '')){
		retval += retvalreceptor;
		retval += retvalDomRec;
	}
	retval += '</fx:Receptor>';
		
	retval = retval.replace(/&/g, "&amp;");
	retval = retval.replace(/"/g, "&quot;");
    return retval;
}






//=============================inicia trama 02=================================================================


function nlGetCFDi02(invoice, customer, params, setupcfdi)
{
    //----->Items
	var retval          = '';
    var wdiscountitem   = invoice.getFieldText('discountitem');
	var wglobaldiscount = Math.abs(parseFloat(isNull2(invoice.getFieldValue('discounttotal'), 0)));
	var wglobalsubtotal = Math.abs(parseFloat(isNull2(invoice.getFieldValue('subtotal'), 0))); 
    var wdiscountrate   = (wglobaldiscount*100/(wglobalsubtotal));
	wdiscountrate       = isNaN(wdiscountrate) ? 0 : parseFloat(wdiscountrate);
	if (wdiscountrate != null && wdiscountrate != '')
	{wdiscountrate = wdiscountrate.toFixed(2);}
    var bandera         = false;
    var j               = 1;
	var wnum_partida    = 0;
	var printitems      = null;
	var amountgroup     = '';
	var discountingroup = 0;
	var ratelinealdiscountingroup = '';
	var subtotal        = 0;
	var subtotalbruto   = 0;    
	var numline         = 0;
	var numlinetax      = 0;
	this.tasades 		= new Array();
	this.tasatax 		= new Array();
	this.TasaRetencion  = new Array();
	var taxexento       = isNull(setupcfdi.getFieldValue('custrecord_cfdi_taxexento'));
	if (taxexento == null || taxexento == ''){taxexento = 0}
	var anticipo        = 0;
	var BanderaAnticipo = false;
	var ItemAnticipo    = invoice.getFieldText('discountitem');
	if (ItemAnticipo == 'Anticipo') { BanderaAnticipo = true;} 
	retval             += '<fx:Conceptos>';
	
	while (j <= invoice.getLineItemCount('item'))
	{
		var discount  = 0; 
		var witemid   = invoice.getLineItemValue('item', 'item', j);
		var xxtype    = invoice.getLineItemValue('item', 'itemtype', j);
		var witemtype = xxtype;
		var line      = parseFloat(invoice.getLineItemValue('item', 'line', j));
		
		if (witemtype == '' && xxtype == 'EndGroup'){
			witemtype = xxtype
		}
		
		//---> Obtiene el valor para ver si se impriman los articulos de grupo en la factura
		if (witemtype == 'Group'){
			var record = nlapiLoadRecord('itemgroup', witemid);
			printitems = record.getFieldValue('printitems');
			line = line+1;
		}
		if (xxtype == 'Group' && printitems == 'T'){
			ratelinealdiscountingroup =  Math.abs(parseFloat(isNull(RateDiscountGroup(j, invoice))));
		}
		if (xxtype == 'EndGroup' && printitems == 'T'){
			ratelinealdiscountingroup =  '';
		}
		
		if (xxtype == 'Group' && printitems == 'T'){	
			j=j+1;		
		}		
		
			
		if (bandera == false)
		{		
			//--> Obtiene Informacion Detalle Factura
			var witemdiscounts = 0;
			var wdiscounts	   = new Array();
			var wtax		   = new Array();
		  	var itemidtext     = invoice.getLineItemText('item', 'item', j);
			var wdescitem      = Trim(invoice.getLineItemValue('item', 'description', j));
			    //wdescitem      = itemidtext + ', ' +wdescitem;
			var descArray      = new Array();
			descArray          = wdescitem.split('\n');
			var wamount        = parseFloat(invoice.getLineItemValue('item', 'amount', j));  
			var wtaxamt        = parseFloat(invoice.getLineItemValue('item', 'tax1amt', j));
			var wquantity      = Math.abs(parseFloat(invoice.getLineItemValue('item', 'quantity', j)));  
			var wrate          = parseFloat(invoice.getLineItemValue('item', 'rate', j)); 		
			var wtaxrate       = parseFloat(invoice.getLineItemValue('item', 'taxrate1', j));
			var taxcode        = parseFloat(invoice.getLineItemValue('item', 'taxcode', j));  
			if (witemtype == 'markupitem') {var wquantity = 1;}
			if (xxtype == 'Group'){
				wtaxrate = parseFloat(invoice.getLineItemValue('item', 'taxrate1', j+1));
			}
			
			var wcodigo    = (invoice.getLineItemValue('item', 'custcol_codigo', j));
			if (wcodigo != null && wcodigo != ''){
				if(wcodigo.length > 30){
				   wcodigo = wcodigo.substr(0,30);
				}
			}
			
			var medida = Trim(invoice.getLineItemText('item', 'units', j));
			var idieps = isNull(setupcfdi.getFieldValue('custrecord_cfdi_idieps'));
			
			if (wquantity == 0){
				wquantity = 1;
				wrate = wamount;	
			}
						
			
			//----> Esto es para los descuentos por linea
			discount = DiscountForItem(line, invoice);		//trae el monto en negativo
			if (discount < 0){	
				wrate = wrate + (discount/wquantity);
			}
			
			if (ratelinealdiscountingroup > 0){
				wrate = wrate*(1-(ratelinealdiscountingroup/100));
			}
		
			
			if (witemtype == 'InvtPart' ||witemtype == 'Assembly' ||witemtype == 'Kit' ||witemtype == 'Group' || witemtype == 'Markup' ||witemtype == 'NonInvtPart' ||witemtype == 'OthCharge' ||witemtype == 'Payment' ||witemtype == 'Service' )
			{
				
				//-----> Numeros de serie
				var wnumparte      = invoice.getLineItemValue('item', 'custcol_numpart', j);
				if (wnumparte != null && wnumparte != ''){
					wnumparte = wnumparte.replace(/\n/g, "");
				}
				var wserialnumbers = invoice.getLineItemValue('item', 'serialnumbers', j); 		
				
				if(witemtype == 'lotnumberedinventoryitem' || witemtype == 'lotnumberedassemblyitem'){
					wserialnumbers = "";
				}
				
				if(wserialnumbers == null || wserialnumbers == "")	{
					var arrseries = ArraySerialNumber(invoice);					
					wserialnumbers = ObtSerialNumbersOrPedimento(arrseries, wnumparte);					
				}
				else{
					wserialnumbers = replace(wserialnumbers, String.fromCharCode(5), ",");					
				}
				
				var xserials = wserialnumbers.split(","); 
				var yserial = AgroupSerials(xserials);				
				
				retval += '<fx:Concepto>';				 
				retval += '<fx:Cantidad>' + isNull(wquantity) + '</fx:Cantidad>'; 
				
				if (medida != null && medida != '')	
				{retval += '<fx:UnidadDeMedida>' + medida + '</fx:UnidadDeMedida>';}
				else
				{retval += '<fx:UnidadDeMedida>No Aplica</fx:UnidadDeMedida>';}
				
				if (wcodigo != null && wcodigo != '')
				{	retval += '<fx:Codigo>' + wcodigo +'</fx:Codigo>';	}
										
				retval += '<fx:Descripcion>' + wdescitem +  '</fx:Descripcion>'; 
				
				var discountmonto = nsoParseFloatOrZero(wdiscountrate/100);
				
				//--------------esto es por si es de grupo y solo se necesita el grupo y su total sin incluir los articulos
				if (witemtype == 'Group' && printitems == 'F')
				{
					for (var i = j+1; i <= invoice.getLineItemCount('item'); i++)
					{
						var wtype = invoice.getLineItemValue('item', 'itemtype', i);
						if (wtype == 'EndGroup')
						{
							amountgroup =  parseFloat(invoice.getLineItemValue('item', 'amount', i));
							break;
						}
					}
					wrate = parseFloat(amountgroup/wquantity);
					
				
					nlapiLogExecution('DEBUG', 'wrate dentro del itemgroup && printitems es falso): ', wrate);
					discountingroup = parseFloat(invoice.getLineItemValue('item', 'amount', i+1));
					if (discountingroup < 0)
					{
						wrate = parseFloat(wrate+discountingroup);
					}
					else
					{discountingroup = 0;}
				}// end of if (witemtype == 'itemgroup' && printitems == 'F')
														
				if (wglobaldiscount != null && wglobaldiscount != '' && wglobaldiscount != "NaN"){
					var ValorUnitario = (wrate - ((wrate*discountmonto)));
					var Importe       = parseFloat(ValorUnitario*wquantity).toFixed(4);
						subtotal      += parseFloat(ValorUnitario*wquantity);
				
					retval += '<fx:ValorUnitario>' + ValorUnitario.toFixed(4) + '</fx:ValorUnitario>'; 	
					retval += '<fx:Importe>' + Importe + '</fx:Importe>';													
				}
				else{
					retval   += '<fx:ValorUnitario>' + wrate.toFixed(4) + '</fx:ValorUnitario>';
					retval   += '<fx:Importe>' + (wrate*wquantity).toFixed(4) + '</fx:Importe>';
					subtotal += parseFloat(wrate*wquantity);
				}
					
				var pedi    = invoice.getLineItemValue('item', 'custcol_pedimentos', j);			
				var arrpedi = new Array();
				
				if(pedi != null && pedi != "")
				{	
					pedi            = nsoReplace(pedi,"-",'');
					arrpedi         = pedi.split(',');										
					var fechaped    = invoice.getLineItemValue('item', 'custcol_fechapedimento', j);
					var arrfechaped = new Array();
					
					if (fechaped != null && fechaped != ''){
						arrfechaped = fechaped.split(',');
					}

					var naduana    = invoice.getLineItemValue('item', 'custcol_aduana', j);
					var arrnaduana = new Array();
					if (naduana != null && naduana != ''){
						arrnaduana = naduana.split(',');
					}
					
					retval += '<fx:Opciones>'; 			
					retval += '<fx:DatosDeImportacion>';
			
			
					for (g = 0; arrpedi != null && g < arrpedi.length; g++)
					{
						retval += '<fx:InformacionAduanera>';
						retval += '<fx:NumeroDePedimento>' + arrpedi[g] + '</fx:NumeroDePedimento>';
						
						if(arrfechaped != null && g < arrfechaped.length){												
							retval += '<fx:FechaDePedimento>' + arrfechaped[g] + '</fx:FechaDePedimento>';						
						}
						
						if(arrnaduana != null && g < arrnaduana.length ){
							retval += '<fx:NombreDeAduana>' + arrnaduana[g] + '</fx:NombreDeAduana>';
						}
						
						retval += '</fx:InformacionAduanera>';						
					}
							
					retval += '</fx:DatosDeImportacion>';
					retval += '</fx:Opciones>'; 			
				}
						
				retval += '<fx:ConceptoEx>';
												
				var base = (wrate*wquantity);
				
				if (witemtype == 'itemgroup' && printitems == 'F')
				{
					var Plista =  parseFloat(amountgroup/wquantity).toFixed(4);
					retval       += '<fx:PrecioLista>' + Plista + '</fx:PrecioLista>'; 														
					retval       += '<fx:ImporteLista>' + parseFloat(amountgroup).toFixed(4) + '</fx:ImporteLista>';	
					subtotalbruto = parseFloat(subtotalbruto) + parseFloat(amountgroup);	
				}
				else
				{
					var preciolista = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'rate', j));
					if (preciolista == 0){
						preciolista = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'amount', j));
					}
					
					retval         += '<fx:PrecioLista>' + preciolista.toFixed(4) + '</fx:PrecioLista>'; 														
					retval         += '<fx:ImporteLista>' + wamount.toFixed(4) + '</fx:ImporteLista>';
					subtotalbruto   = parseFloat(subtotalbruto) + parseFloat(wamount);
				}
																	
				var discountamount = Math.abs(parseFloat(discount - anticipo));
				
				//-------> Con descuento lineal o anticipo lineal, pero no global
				if (( discount < 0 || ratelinealdiscountingroup > 0 || discountingroup < 0 || anticipo < 0)&&(wglobaldiscount == null || wglobaldiscount == '' || wglobaldiscount == 'NaN'))
				{					
					if (ratelinealdiscountingroup > 0){	 
						wrate = parseFloat( wamount/wquantity);
						discountamount = Math.abs(parseFloat(wamount*(ratelinealdiscountingroup/100)));
					}
					if ( discountingroup < 0){	
						wamount = amountgroup; 
						wrate = parseFloat(wamount/wquantity);
						discountamount = Math.abs(parseFloat(discountingroup));						
					}										
					if ((wrate*wquantity) >= 0)
					{


						nlapiLogExecution('DEBUG', 'Partida con descuento lineal, pero no global:  ', '');
						retval += '<fx:DescuentosYRecargos>';

						if (discountamount >= 0)
						{
							retval += '<fx:DescuentoORecargo>';
							retval += '<fx:Operacion>DESCUENTO</fx:Operacion>';
							retval += '<fx:Imputacion>FUERA_DE_FACTURA</fx:Imputacion>';
							retval += '<fx:Servicio>DESCUENTO</fx:Servicio>';
							retval += '<fx:Base>' + parseFloat(wamount).toFixed(2) + '</fx:Base>';	
							
							var tasa1 = nsoParseFloatOrZero((discountamount*100)/wamount).toFixed(2);
							if (ratelinealdiscountingroup > 0){
								tasa1 = ratelinealdiscountingroup.toFixed(2);
								discountamount = parseFloat((wrate*wquantity)*(tasa1/100));
							}
							retval += '<fx:Tasa>' + tasa1 + '</fx:Tasa>';
							retval += '<fx:Monto>' + discountamount.toFixed(2) + '</fx:Monto>';
							retval += '</fx:DescuentoORecargo>';
							
							
							var groupdes = new TasaDescuentos(tasa1, parseFloat(wamount).toFixed(2), discountamount.toFixed(2));
							this.tasades.splice(numline, 0, new Array());
							this.tasades[numline] = groupdes;
							numline += 1;
						}

						//anticipos
						if (anticipo < 0)
						{
							nlapiLogExecution('DEBUG', 'Anticipo lineal:  ', '');
							
							retval += '<fx:DescuentoORecargo>';
							retval += '<fx:Operacion>DESCUENTO</fx:Operacion>';
							retval += '<fx:Imputacion>FUERA_DE_FACTURA</fx:Imputacion>';
							retval += '<fx:Servicio>DESCUENTO</fx:Servicio>';
							retval += '<fx:Descripcion>ANTICIPO</fx:Descripcion>';
							retval += '<fx:Base>' + parseFloat(wamount).toFixed(2) + '</fx:Base>';	
							var tasanticipo = nsoParseFloatOrZero((anticipo*100)/wamount).toFixed(2);

							retval += '<fx:Tasa>' + Math.abs(tasanticipo) + '</fx:Tasa>';
							retval += '<fx:Monto>' + Math.abs(anticipo).toFixed(2) + '</fx:Monto>';
							retval += '</fx:DescuentoORecargo>';
							
							
							var groupdes = new TasaDescuentos(Math.abs(tasanticipo), parseFloat(wamount).toFixed(2), Math.abs(anticipo).toFixed(2));
							this.tasades.splice(numline, 0, new Array());
							this.tasades[numline] = groupdes;
							numline += 1;
						}

							retval += '</fx:DescuentosYRecargos>';
						
						if (taxcode != taxexento)
						{
							retval += '<fx:Impuestos>';
							retval += '<fx:Impuesto>';
							retval += '<fx:Contexto>' + 'FEDERAL'+ '</fx:Contexto>';	
							retval += '<fx:Operacion>' + 'TRASLADO'+ '</fx:Operacion>';	
							retval += '<fx:Codigo>' + 'IVA'+ '</fx:Codigo>';	
							retval += '<fx:Base>' + (wamount-discountamount + anticipo).toFixed(2) + '</fx:Base>';	
							retval += '<fx:Tasa>' + nsoParseFloatOrZero(wtaxrate) + '</fx:Tasa>';
							var monto1= nsoParseFloatOrZero((wamount-discountamount + anticipo)*(wtaxrate/100)).toFixed(2);
							retval += '<fx:Monto>' + monto1 + '</fx:Monto>';
							retval += '</fx:Impuesto>';
							retval += '</fx:Impuestos>'	
							
							
							/*if (amountretencion > 0)
							{
								retval += '<fx:Impuesto>';
								retval += '<fx:Contexto>' + 'FEDERAL'+ '</fx:Contexto>';	
								retval += '<fx:Operacion>' + 'RETENCION'+ '</fx:Operacion>';	
								retval += '<fx:Codigo>' + tiporetencion + '</fx:Codigo>';	
								retval += '<fx:Base>' + (wamount-discountamount + anticipo).toFixed(2) + '</fx:Base>';	
								retval += '<fx:Tasa>' + rateretencion.toFixed(2) + '</fx:Tasa>';
								retval += '<fx:Monto>' + amountretencion.toFixed(2) + '</fx:Monto>';
								retval += '</fx:Impuesto>';
							}	
							retval += '</fx:Impuestos>'	*/
							
						}
					}
					
				}
				
				//----> con descuento global, pero no lineal	
				if ((wglobaldiscount != null && wglobaldiscount != '' )&&( discount == 0 )&&(discountingroup>=0 || isNaN(discountingroup)) && (ratelinealdiscountingroup == null || ratelinealdiscountingroup == '' || ratelinealdiscountingroup == 'NaN' || isNaN(ratelinealdiscountingroup)))
				{
					if (base > 0)
					{
						nlapiLogExecution('DEBUG', 'Partida con descuento global, pero no lineal	:  ', '');
						retval += '<fx:DescuentosYRecargos>';
						retval += '<fx:DescuentoORecargo>';
						retval += '<fx:Operacion>DESCUENTO</fx:Operacion>';
						retval += '<fx:Imputacion>FUERA_DE_FACTURA</fx:Imputacion>';
						retval += '<fx:Servicio>DESCUENTO</fx:Servicio>';
						retval += '<fx:Base>' + base.toFixed(2) + '</fx:Base>';	
						retval += '<fx:Tasa>' + wdiscountrate + '</fx:Tasa>';
						var monto2= ((base*wdiscountrate)/100).toFixed(2);
						retval += '<fx:Monto>' + monto2 + '</fx:Monto>';
						retval += '</fx:DescuentoORecargo>';
						retval += '</fx:DescuentosYRecargos>';
						
						var groupdes = new TasaDescuentos(wdiscountrate, base, monto2);
						this.tasades.splice(numline, 0, new Array());
						this.tasades[numline] = groupdes;
						numline += 1;
						
						if (taxcode != taxexento)
						{
							retval += '<fx:Impuestos>';
							retval += '<fx:Impuesto>';
							retval += '<fx:Contexto>' + 'FEDERAL'+ '</fx:Contexto>';	
							retval += '<fx:Operacion>' + 'TRASLADO'+ '</fx:Operacion>';	
							retval += '<fx:Codigo>' + 'IVA'+ '</fx:Codigo>';
							var base2 = nsoParseFloatOrZero(base-((base*wdiscountrate)/100)).toFixed(2);	
							retval += '<fx:Base>' + base2 + '</fx:Base>';	
							retval += '<fx:Tasa>' + nsoParseFloatOrZero(wtaxrate) + '</fx:Tasa>';
							var monto2 = nsoParseFloatOrZero((base-((base*wdiscountrate)/100))*(wtaxrate/100)).toFixed(2);
							retval += '<fx:Monto>' + monto2 + '</fx:Monto>';
							retval += '</fx:Impuesto>';
							retval += '</fx:Impuestos>'	
						}
					}										
				}
					
				//-----> con ambos descuentos  				
				if (wglobaldiscount != null && wglobaldiscount != '' &&( discount<0  || discountingroup<0 || ratelinealdiscountingroup > 0))
				{
					nlapiLogExecution('DEBUG', 'con ambos descuentos en la partida	:  ', '');
					if (witemtype == 'itemgroup' && printitems == 'F'){
						wamount = amountgroup;
					 	discountamount = Math.abs(parseFloat(discountingroup));					
					}
					if (printitems == 'T'){
						discountamount = Math.abs(parseFloat(wamount*(ratelinealdiscountingroup/100)));		
					}
										
					
					var tasacompuesta = parseFloat((ValorUnitario*100)/(wamount/wquantity)).toFixed(2);
					tasacompuesta     = nsoParseFloatOrZero(100-tasacompuesta);
					var monto3        = nsoParseFloatOrZero(wamount*(tasacompuesta/100));
					
					retval += '<fx:DescuentosYRecargos>';
					retval += '<fx:DescuentoORecargo>';
					retval += '<fx:Operacion>DESCUENTO</fx:Operacion>';
					retval += '<fx:Imputacion>FUERA_DE_FACTURA</fx:Imputacion>';
					retval += '<fx:Servicio>DESCUENTO</fx:Servicio>';
					retval += '<fx:Base>' + wamount.toFixed(4) + '</fx:Base>';	
					retval += '<fx:Tasa>' + nsoParseFloatOrZero(tasacompuesta).toFixed(2) + '</fx:Tasa>';
					retval += '<fx:Monto>' + monto3.toFixed(2) + '</fx:Monto>';
					retval += '</fx:DescuentoORecargo>';
					retval += '</fx:DescuentosYRecargos>';												
					
					var groupdes = new TasaDescuentos(tasacompuesta, wamount, monto3);
					this.tasades.splice(numline, 0, new Array());
					this.tasades[numline] = groupdes;
					numline += 1;						
					
					if (taxcode != taxexento)
					{
						retval += '<fx:Impuestos>';
						retval += '<fx:Impuesto>';
						retval += '<fx:Contexto>' + 'FEDERAL'+ '</fx:Contexto>';	
						retval += '<fx:Operacion>' + 'TRASLADO'+ '</fx:Operacion>';	
						retval += '<fx:Codigo>' + 'IVA'+ '</fx:Codigo>';	
						retval += '<fx:Base>' + nsoParseFloatOrZero(Importe) + '</fx:Base>';	
						retval += '<fx:Tasa>' + nsoParseFloatOrZero(wtaxrate) + '</fx:Tasa>';
						var montotax = nsoParseFloatOrZero(Importe*(wtaxrate/100)).toFixed(2);
						retval += '<fx:Monto>' + montotax + '</fx:Monto>';
						retval += '</fx:Impuesto>';
						retval += '</fx:Impuestos>'	
					}
						
				}									
	
				//-----> Impuestos sin descuentos
				if (discount == 0  && wglobaldiscount == 0 && (discountingroup == 0 || isNaN(discountingroup) )&& (ratelinealdiscountingroup == null || ratelinealdiscountingroup == '' || ratelinealdiscountingroup == 'NaN' || isNaN(ratelinealdiscountingroup)))
				{				
					if (taxcode != taxexento){
							if (witemtype == 'itemgroup' && printitems == 'F')
							{wamount = parseFloat(amountgroup).toFixed(2)}
							
							retval += '<fx:Impuestos>';
							retval += '<fx:Impuesto>';
							retval += '<fx:Contexto>' + 'FEDERAL'+ '</fx:Contexto>';	
							retval += '<fx:Operacion>' + 'TRASLADO'+ '</fx:Operacion>';	
							retval += '<fx:Codigo>' + 'IVA'+ '</fx:Codigo>';	
							retval += '<fx:Base>' + nsoParseFloatOrZero(wamount).toFixed(2) + '</fx:Base>';	
							retval += '<fx:Tasa>' + nsoParseFloatOrZero(wtaxrate).toFixed(2) + '</fx:Tasa>';
							var monto4 = nsoParseFloatOrZero((wamount)*(wtaxrate/100)).toFixed(2);
							retval += '<fx:Monto>' + monto4 + '</fx:Monto>';
							retval += '</fx:Impuesto>';
							retval += '</fx:Impuestos>'	
						}
				}
					
				if (yserial != null && yserial != ''){	
					retval += '<fx:NumeroDeSerie>' + isNull(yserial) + '</fx:NumeroDeSerie>';		
				}
				
				retval += '</fx:ConceptoEx>';
				retval += '</fx:Concepto>';
					
			}//end of if (witemtype == 'inventoryitem' || witemtype == 'assemblyitem' || ..........
		
		} // end of if (bandera = false)
        
		//-----> si es F es que solo salga el grupo y su total
		if (xxtype == 'Group' && printitems == 'F'){
			bandera = true;		
		}
		if (witemtype == 'EndGroup' || xxtype == 'EndGroup'){	
			bandera = false;	
			ratelinealdiscountingroup =  '';
		}			
		if (bandera == true){	
			var amountgrup = parseFloat(nlGetAmountGroup(invoice, bandera));		
		}
		
        j++;

    } //end while
	
	this.subtotal      = subtotal;
	this.subtotalbruto = subtotalbruto;

	retval = retval.replace(/&/g, "&amp;");
	retval = retval.replace(/"/g, "&quot;");
    retval += '</fx:Conceptos>';
	
    return retval;
		
}



//-------------------------------> Inicia trama totales    <--------------------------------// 
function Totales(invoice, customer, params, setupcfdi) 
{
	
    var wglobaldiscount = Math.abs(parseFloat(isNull2(invoice.getFieldValue('discounttotal'), 0)));	
	var tipodecambio    = Math.abs(parseFloat(isNull2(invoice.getFieldValue('exchangerate')))).toFixed(6);
	var subtotal        = Math.abs(parseFloat(invoice.getFieldValue('subtotal'))); 
	var wdiscountrate   = isNull((wglobaldiscount*100)/subtotal);
	wdiscountrate       = isNaN(wdiscountrate) ? 0 : parseFloat(wdiscountrate);
	var taxexento       = isNull(setupcfdi.getFieldValue('custrecord_cfdi_taxexento'));
	if (taxexento == null || taxexento == ''){taxexento = 0}
	
	if (wdiscountrate != null && wdiscountrate != ''){
		wdiscountrate = wdiscountrate.toFixed(2);
	}
	
	var montodescuento = isNull((subtotal*wdiscountrate)/100);	
	var newamount      = 0;
	var subtotalbruto  = 0;
	var totaldescuentoporlinea = 0;
	var poswamount     = 0; 
	var taxrate	       = isNull(nlGetGlobalTaxRate(invoice));
	var taxtotal       = parseFloat(invoice.getFieldValue('taxtotal'));	
	var taxamount      = 0;
	var taxceroamount  = 0;
	
	if(taxtotal != 0){
		taxamount = parseFloat((taxtotal*100)/taxrate);
	}
	
	var tasacompuesta = 0;
				
	var retval = '<fx:Totales>';	                                                   
	retval += '<fx:Moneda>' + isNull(invoice.getFieldValue('currencysymbol')) + '</fx:Moneda>'; 												   
	retval += '<fx:TipoDeCambioVenta>' + tipodecambio + '</fx:TipoDeCambioVenta>';	
	retval += '<fx:SubTotalBruto>' + parseFloat(this.subtotalbruto).toFixed(2) + '</fx:SubTotalBruto>';
	retval += '<fx:SubTotal>' + parseFloat(this.subtotal).toFixed(2) + '</fx:SubTotal>';
				
	var Descgroup = AgrupaDescTipoA();
	var tdiscount = 0;
	var count     = 1;
	var existdiscount = '';
	
	if(Descgroup)
	{					
		for (h in Descgroup)
		{	
			if (count == 1)	
			{
				retval += '<fx:DescuentosYRecargos>';
				existdiscount = true;
			}
			var wxamount      = 0;
			var wbase         = 0;
			var wrate         = 0;			
			
			for (k in Descgroup[h])
			{							
				wxamount  += nsoParseFloatOrZero(Descgroup[h][k].monto); 
				wbase     += nsoParseFloatOrZero(Descgroup[h][k].base);				
				wrate      = nsoParseFloatOrZero(Descgroup[h][k].rate); 
												
			}
			tdiscount += wxamount;	
			
			retval += '<fx:DescuentoORecargo>';
			retval += '<fx:Operacion>DESCUENTO</fx:Operacion>';
			retval += '<fx:Imputacion>FUERA_DE_FACTURA</fx:Imputacion>';
			retval += '<fx:Servicio>DESCUENTO</fx:Servicio>';
			retval += '<fx:Base>'  + wbase.toFixed(2) + '</fx:Base>';	
			retval += '<fx:Tasa>'  + wrate + '</fx:Tasa>';
			retval += '<fx:Monto>' + wxamount.toFixed(2) + '</fx:Monto>';
			retval += '</fx:DescuentoORecargo>';
			count +=1;
		}
		
		if (existdiscount == true)	
		{
			retval += '</fx:DescuentosYRecargos>';
		}
				
	}//end Descgroup
		
		
	retval += '<fx:ResumenDeDescuentosYRecargos>';	
	retval += '<fx:TotalDescuentos>' + nsoParseFloatOrZero(tdiscount).toFixed(2) + '</fx:TotalDescuentos>';			
	retval += '<fx:TotalRecargos>0.00</fx:TotalRecargos>';
	retval += '</fx:ResumenDeDescuentosYRecargos>';	
							
	var taxgroup = AgrupaIVA(invoice, taxexento);
	
	//-----> inicia la parte de Impuestos esto es general en los totales
	
	var retvalTax = '';
	for (h in taxgroup)
	{				
		var wxamount  = 0;
		
		for (k in taxgroup[h]){
			var wtax     = taxgroup[h][k].tax; 
			var wtaxcode = taxgroup[h][k].taxcode;
			var wxamount = wxamount + taxgroup[h][k].amount; 
		}

		if (wdiscountrate > 0){
			wxamount = wxamount*(1-(wdiscountrate/100));
		}
		
			retvalTax += '<fx:Impuesto>';
			retvalTax += '<fx:Contexto>' + 'FEDERAL'+ '</fx:Contexto>';
			retvalTax += '<fx:Operacion>' + 'TRASLADO'+ '</fx:Operacion>';
			retvalTax += '<fx:Codigo>' + 'IVA'+ '</fx:Codigo>';
			retvalTax += '<fx:Base>' + nsoParseFloatOrZero(wxamount).toFixed(2) + '</fx:Base>';
			retvalTax += '<fx:Tasa>' + wtax + '</fx:Tasa>';
			var montotax = nsoParseFloatOrZero(wtax*(wxamount/100)).toFixed(2)
			retvalTax += '<fx:Monto>' + montotax + '</fx:Monto>';
			retvalTax += '</fx:Impuesto>';	
		
	}
	
	var ivaretenido = AgrupaRetencion();
	var retvalTaxRet = '';
	if (ivaretenido)
	{
		for (h in ivaretenido)
		{
			var wxamount  = 0;
			
			for (k in ivaretenido[h])
			{
				var wrate     = ivaretenido[h][k].rate; 
				var wtype     = ivaretenido[h][k].tipo; 
				var wxamount  = wxamount + ivaretenido[h][k].monto; 
			}
	
			var wbase = nsoParseFloatOrZero((wxamount*100)/wrate).toFixed(2);
	
			retvalTaxRet += '<fx:Impuesto>';
			retvalTaxRet += '<fx:Contexto>' + 'FEDERAL'+ '</fx:Contexto>';
			retvalTaxRet += '<fx:Operacion>' + 'RETENCION'+ '</fx:Operacion>';
			retvalTaxRet += '<fx:Codigo>' + wtype+ '</fx:Codigo>';
			retvalTaxRet += '<fx:Base>' + wbase + '</fx:Base>';	
			retvalTaxRet += '<fx:Tasa>' + wrate.toFixed(2) + '</fx:Tasa>';
			retvalTaxRet += '<fx:Monto>' + wxamount.toFixed(2) + '</fx:Monto>';
			retvalTaxRet += '</fx:Impuesto>';
		}
		
		
	}// end of if (ivaretenido)
	
	if ((retvalTaxRet != '' && retvalTaxRet != null)||(retvalTax != '' && retvalTax != null))
	{
		retval += '<fx:Impuestos>';
		retval += retvalTax;
		retval += retvalTaxRet;
		retval += '</fx:Impuestos>';
	}
	
	var TotalIVARetenido = nsoParseFloatOrZero(TotalRetencionesIVA(invoice));
	var TotalISRRetenido = nsoParseFloatOrZero(TotalRetencionesISR(invoice));
	
	var TotalRetencionesFederales = nsoParseFloatOrZero(TotalIVARetenido) + nsoParseFloatOrZero(TotalISRRetenido);
	
	retval += '<fx:ResumenDeImpuestos>';		
	retval += '<fx:TotalTrasladosFederales>' + nsoParseFloatOrZero(taxtotal) + '</fx:TotalTrasladosFederales>';
	retval += '<fx:TotalIVATrasladado>' + nsoParseFloatOrZero(taxtotal) + '</fx:TotalIVATrasladado>';
	retval += '<fx:TotalIEPSTrasladado>' + '0' + '</fx:TotalIEPSTrasladado>';
	retval += '<fx:TotalRetencionesFederales>' + TotalRetencionesFederales + '</fx:TotalRetencionesFederales>';
	retval += '<fx:TotalISRRetenido>' + TotalISRRetenido + '</fx:TotalISRRetenido>';
	retval += '<fx:TotalIVARetenido>' + TotalIVARetenido + '</fx:TotalIVARetenido>';
	retval += '<fx:TotalTrasladosLocales>' + '0' + '</fx:TotalTrasladosLocales>';
	retval += '<fx:TotalRetencionesLocales>' + '0' + '</fx:TotalRetencionesLocales>';	
	retval += '</fx:ResumenDeImpuestos>';	
	retval += '<fx:Total>' + isNull(invoice.getFieldValue('total')) + '</fx:Total>';		
	retval += '<fx:TotalEnLetra>' + isNull(invoice.getFieldValue('custbody_amount_words')) + '</fx:TotalEnLetra>';		
	retval += '<fx:FormaDePago>' + isNull(invoice.getFieldText('custbody_cfdi_formadepago')) + '</fx:FormaDePago>'; 		
	retval += '</fx:Totales>';

	return retval;
}




//================================== Comprobante Ex =======================================


function ComprobanteEx (invoice, customer, params, setupcfdi)
{
	var retval          = '';
	var metododepago    = invoice.getFieldValue('custbody_cfdi_metpago_sat');
	var terms           = invoice.getFieldText('terms');
	var leyendasfisc    = invoice.getFieldValue('custbody_cfdi_leyfiscales');
	var memo            = invoice.getFieldValue('memo');
	var testing         = setupcfdi.getFieldValue('custrecord_cfdi_testing');
	var custid          = invoice.getFieldValue("entity");
	if (leyendasfisc != null && leyendasfisc != ''){
	 	retval += '<fx:Complementos>';
		 	retval += '<fx:LeyendasFiscales>';
			 	retval += '<fx:Leyenda>';
				 	retval += '<fx:TextoLeyenda>' + leyendasfisc + '</fx:TextoLeyenda>';
				retval += '</fx:Leyenda>';
			retval += '</fx:LeyendasFiscales>';
		retval += '</fx:Complementos>';
	}
	
	retval += '<fx:ComprobanteEx>';
	retval += '<fx:DatosDeNegocio>';
	if (testing == 'T')
	{retval += '<fx:Sucursal>' + setupcfdi.getFieldValue('custrecord_cfdi_sucursal_testing') + '</fx:Sucursal>'; }
	else
	{retval += '<fx:Sucursal>' + params.getFieldValue('custrecord_cfdi_sucursal_mysuite') + '</fx:Sucursal>';}
	retval += '</fx:DatosDeNegocio>';
	var claveMetodoPago = '99';	
	nlapiLogExecution('DEBUG','metododepago: ' + metododepago);
	if (metododepago != null && metododepago != '')	{
		claveMetodoPago = MetPago(metododepago);
	}
	retval += '<fx:TerminosDePago>';
	retval += '<fx:MetodoDePago>' + claveMetodoPago + '</fx:MetodoDePago>';
	retval += '</fx:TerminosDePago>';
		
		
	var shipaddresslist = invoice.getFieldValue("shipaddresslist");

	for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++) {
		
		if (customer.getLineItemValue("addressbook", "id", i) == shipaddresslist){
					
			retval += '<fx:DatosDeEmbarque>';
			retval += '<fx:LugarDeEntrega>';
			
			var nombreenvio = isNull(customer.getLineItemValue('addressbook', 'attention', i));
			nombreenvio = nombreenvio.replace(/&/g, "&amp;");
			var calleenvio = isNull(customer.getLineItemValue('addressbook', 'addressee', i));
			var coloniaenvio = isNull(customer.getLineItemValue('addressbook', 'addr1', i));
			var municipioenvio =  isNull(customer.getLineItemValue('addressbook', 'addr2', i));
			var estadoenvio = isNull(customer.getLineItemValue('addressbook', 'state', i)) + ', ' + isNull(customer.getLineItemValue('addressbook', 'city', i));
			var cpenvio = isNull2(customer.getLineItemValue('addressbook', 'zip', i));
			
			if (nombreenvio != null && nombreenvio != '')
			{retval += '<fx:Nombre>' + nombreenvio + '</fx:Nombre>';}
			retval += '<fx:Domicilio>';
			if (calleenvio != null && calleenvio != '')
			{retval += '<fx:Calle>' + calleenvio + '</fx:Calle>';}
			if(coloniaenvio != null && coloniaenvio != '')
			{retval += '<fx:Colonia>' + coloniaenvio + '</fx:Colonia>';}
			if (municipioenvio!= null && municipioenvio != '')
			{retval += '<fx:Municipio>' +  municipioenvio + '</fx:Municipio>';}
			if (estadoenvio != null && estadoenvio != '')
			{retval += '<fx:Estado>' + estadoenvio + '</fx:Estado>';}
			retval += '<fx:Pais>' + isNull(nsoGetCountryName(customer.getLineItemValue('addressbook', 'country', i))) + '</fx:Pais>';
			if (cpenvio != null && cpenvio != '')
			{retval += '<fx:CodigoPostal>' + cpenvio + '</fx:CodigoPostal>';}
			retval += '</fx:Domicilio>';	
			retval += '</fx:LugarDeEntrega>';
			retval += '</fx:DatosDeEmbarque>';
		}
	}
		
	
	if((memo != null && memo != ''))
	{
		retval += '<fx:TextosDePie>';
			retval += '<fx:Texto>'+ isNull(memo) + '</fx:Texto>';
		retval += '</fx:TextosDePie>';
	}

	retval += '</fx:ComprobanteEx>';
	retval += '</fx:FactDocMX>'; 
	retval = retval.replace(/&/g, "&amp;");
	retval = retval.replace(/"/g, "&quot;");
	return retval;

}
  		
		

//=============================== Fin Comprobante Ex ========================================


function MetPago(idMetdPago)
{
	var retval = '';
	if(idMetdPago){
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_cfdi_payment_met_nat', null, 'anyof', idMetdPago));
		   
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_cfdi_payment_met_sat'));

		var searchresult  = nlapiSearchRecord('customrecord_cfdi_metododepago', null, filters, columns);
		nlapiLogExecution('DEBUG','searchresult: ' + searchresult);
		for(var i = 0 ; searchresult != null && i < searchresult.length; i++){
			var idMet          = searchresult[i].getText('custrecord_cfdi_payment_met_sat');
			if(idMet != null && idMet != ''){
				var arrayMP = new Array();
				arrayMP = idMet.split('-');
				if(arrayMP.length > 1){
					retval = arrayMP[0];
				}
			}
		}
	}

	return retval;

}



function TotalRetencionesIVA(invoice)
{
	var amount = 0;
	var retval = 0;
	
	for (var j=1 ; j <= invoice.getLineItemCount('item'); j++)
	{
		var subitemof = invoice.getLineItemValue('item', 'custcol_cfdi_retencion', j);
		
		if (subitemof == idretIVA)
		{
			amount = Math.abs(parseFloat(invoice.getLineItemValue('item', 'amount', j)));
			retval += amount
		}
	}
	return retval;
}

function TotalRetencionesISR(invoice)
{
	var amount = 0;
	var retval = 0;
	
	for (var j=1 ; j <= invoice.getLineItemCount('item'); j++)
	{
		var subitemof = invoice.getLineItemValue('item', 'custcol_cfdi_retencion', j);
		
		if (subitemof == idretISR)
		{
			amount = Math.abs(parseFloat(invoice.getLineItemValue('item', 'amount', j)));
			retval += amount
		}
	}
	return retval;
}

function RetencionesInLine(line, record)
{
	var arrayretencion = '';
	
	for (var j=1; j <= record.getLineItemCount('item'); j++)
	{
		var discline = parseFloat(record.getLineItemValue('item', 'discline', j));
		var subitemof = (record.getLineItemValue('item', 'custcol_cfdi_retencion', j));
		
		if ((discline == line) && (subitemof == idretIVA))
		{
			arrayretencion = new Array();
		    var amount = parseFloat(record.getLineItemValue('item', 'amount', j));
			var rate = parseFloat(record.getLineItemValue('item', 'rate', j));
			
			arrayretencion[0] = amount;
			arrayretencion[1] = rate;
			arrayretencion[2] = 'IVA';
		}
		if ((discline == line) && (subitemof == idretISR))
		{

			 arrayretencion = new Array();
		     var amount = parseFloat(record.getLineItemValue('item', 'amount', j));
			 var rate = parseFloat(record.getLineItemValue('item', 'rate', j));
			 
			 arrayretencion[0] = amount;
			 arrayretencion[1] = rate;
			 arrayretencion[2] = 'ISR';
		}

	}

	return arrayretencion;
}

function AgrupaRetencion()
{
	var arrTax = new Array();
	
	for(var i = 0; i < this.TasaRetencion.length; i++)
	{		
		var rate    = this.TasaRetencion[i].rate;
		var indrate = rate * 100;
		var monto   = this.TasaRetencion[i].monto;
		var tipo    = this.TasaRetencion[i].tipo		
		if (indrate != null && indrate != "" )
		{	
			var index = null;
				
			try
			{
				index = arrTax[indrate].length;
			}
			catch(err)
			{			
			}
		
			if (index == null)
			{
				arrTax[indrate] = new Array();
			}
			
			index = arrTax[indrate].length;								
			arrTax[indrate][index] = new RetA(rate, monto, tipo);
		}			
	}
	
	return arrTax;
}




function RetA(rate, monto, tipo)
{
	this.rate = rate;	
	this.monto = nsoParseFloatOrZero(this.monto) + nsoParseFloatOrZero(monto);	
	this.tipo = tipo;	
}


function TasaRetencion(rate, monto, tipo)
{
	this.rate  = rate;	
	this.monto = monto;	
	this.tipo  = tipo;	
}

function TotalAnticipos(record)
{
	var amount = 0;
	
	for (var j=1 ; j <= record.getLineItemCount('item'); j++)
	{
		var itemtype = (record.getLineItemValue('item', 'itemtype', j));

		if ( itemtype == 'Discount')
		{
			var itemid = (record.getLineItemValue('item', 'item', j));
			var upccode   = nlapiLookupField('item', itemid, 'upccode');
			if (upccode == 'Anticipo' )
			{
				amount += nsoParseFloatOrZero(record.getLineItemValue('item', 'amount', j));
			}
		}
	}
	
	return amount;
}



function AgrupaDescTipoA()
{
	var arrTax = new Array();
	
	for(var i = 0; i < this.tasades.length; i++)
	{		
		var rate    = this.tasades[i].rate;
		var indrate = rate * 100;
		var base    = this.tasades[i].base;
		var monto   = this.tasades[i].monto;
				
		if (indrate != null && indrate != "" )
		{	
			var index = null;
				
			try
			{
				index = arrTax[indrate].length;
			}
			catch(err)
			{			
			}
		
			if (index == null)
			{
				arrTax[indrate] = new Array();
			}
			
			index = arrTax[indrate].length;								
			arrTax[indrate][index] = new DesA(rate, base, monto);
		}			
	}
	
	return arrTax;
}


function DesA(rate, base, monto)
{
	this.rate = rate;	
	this.monto = nsoParseFloatOrZero(this.monto) + nsoParseFloatOrZero(monto);	
	this.base  = nsoParseFloatOrZero(this.base) + nsoParseFloatOrZero(base);	
}

function TasaDescuentos(rate, base, monto)
{
	this.base = base;
	this.rate = rate;	
	this.monto = monto;	
}


function AgrupaIVA(record, taxexento)
{
	var arrTax = new Array();
	
	for(var i = 1; record != null && i <= record.getLineItemCount('item'); i++)
	{
		var tax       = parseFloat(record.getLineItemValue('item', 'taxrate1', i)); 	
		var amount    = parseFloat(record.getLineItemValue('item', 'amount', i)); 	
		var taxcode   = record.getLineItemValue('item', 'taxcode', i); 	
		var typeItem  = record.getLineItemValue('item', 'itemtype', i); 			
									
		if (taxcode != null && taxcode != "" && typeItem != 'EndGroup' && typeItem != 'Group' && typeItem != 'Description' && taxcode != taxexento)
		{						
			var index = null;
				
			try
			{
				index = arrTax[taxcode].length;
			}
			catch(err)
			{			
			}
		
			if (index == null)
			{
				arrTax[taxcode] = new Array();
			}
			
			index = arrTax[taxcode].length;								
			arrTax[taxcode][index] = new Tax(tax, taxcode, amount);
		}		
	}
	
	return arrTax;
}


function Tax(tax, taxcode, amount)
{
	this.tax = tax;
	this.taxcode = taxcode;
	this.amount = amount;	
}

function rtrimcoma(s) 
{
	
	var retval = "";
	
    // Quita los espacios en blanco del final de la cadena
	
	if(s != null && s != "")
	{
		var j = 0;
	
		// Busca el �ltimo caracter <> de un espacio
		for (var i = s.length - 1; i > -1; i--)
			if (s.substring(i, i + 1) != ',') {
			j = i;
			break;
		}
		
		retval = s.substring(0, j + 1);
	}
	
	return retval;
}




function nlGetGlobalTaxRate(invoice)
{
	var retval = 0;
	
	for(var i = 1; i <= invoice.getLineItemCount("item"); i++)
	{
		var taxrate = nsoParseFloatOrZero(invoice.getLineItemValue("item", "taxrate1", i));
		//esta funcion convierte el string en numero, en este caso es de 16.0% a 16
		if(taxrate >= retval)
		{
			retval = taxrate;
		}
	}
	
	retval = retval.toFixed(2);
	//esta funcion "tofixed(2)" sirve para ponerle los 2 decimales al numero 
	return retval;
}




function isNull(value) {
    return (value == null) ? '' : value;
}

function isNull2(value, replaceby) {
    return (value == null) ? replaceby : value;
}

function LTrim(s) {
	
	var retval = "";
	
	if(s != null && s != "")
	{
		// Devuelve una cadena sin los espacios del principio
		var i = 0;
		var j = 0;
	
		// Busca el primer caracter <> de un espacio
		for (i = 0; i <= s.length - 1; i++)
			if (s.substring(i, i + 1) != ' ' && s.substring(i, i + 1) != '') {
			j = i;
			break;
		}
		
		retval =  s.substring(j, s.length);
	}
	
	return retval;
}
function RTrim(s) {
	
	var retval = "";
	
    // Quita los espacios en blanco del final de la cadena
	
	if(s != null && s != "")
	{
		var j = 0;
	
		// Busca el �ltimo caracter <> de un espacio
		for (var i = s.length - 1; i > -1; i--)
			if (s.substring(i, i + 1) != ' ' && s.substring(i, i + 1) != '') {
			j = i;
			break;
		}
		
		retval = s.substring(0, j + 1);
	}
	
	return retval;
}
function Trim(s) {
    // Quita los espacios del principio y del final
    return LTrim(RTrim(s));
}


function getTipoDocumento(recordtype) {
    var retval = '';

    switch (recordtype) {
        
        case 'creditmemo':
            retval = 'NOTA_DE_CREDITO'
            break;
		case  'invoice':
			retval = 'FACTURA';
			break;
		case  'cashsale':
			retval = 'CashSale';
			break;
    }

    return retval;
}

function setTipoComprobante(recordtype) {
    var retval = '';

    switch (recordtype) {
        case 'invoice':
            retval = 'ingreso';
            break;
		case 'cashsale':
			retval = 'ingreso';
			break;
        case 'creditmemo':
            retval = 'egreso'
            break;
    }

    return retval;
}



function replace(texto, s1, s2) {
	try{
    	return texto.split(s1).join(s2);
	}
	catch(err)
	{
		
	}
}
	
function fmtInt(number, digits)
{
	var wnumber = number.toString();
	
	if (wnumber.length < digits)
	{
		for (var i = 1; i <= digits - number.toString().length; i++)
		{
		wnumber = "0" + wnumber.toString();
		}
	}

return wnumber;
}


function getDiscountRate(value) {
    if (value == null || value == '') return 0;
    var arr_value = value.split('%');
    return arr_value[0];
}


//--------------------------------------------------------
// ------------------- String functions ------------------
//--------------------------------------------------------
function InStr(n, s1, s2) {
	// Devuelve la posici�n de la primera ocurrencia de s2 en s1
	// Si se especifica n, se empezar� a comprobar desde esa posici�n
	// Sino se especifica, los dos par�metros ser�n las cadenas
	var numargs=InStr.arguments.length;
	
	if(numargs<3)
		return n.indexOf(s1)+1;
	else
		return s1.indexOf(s2, n)+1;
}
function RInStr(n, s1, s2){
	// Devuelve la posici�n de la �ltima ocurrencia de s2 en s1
	// Si se especifica n, se empezar� a comprobar desde esa posici�n
	// Sino se especifica, los dos par�metros ser�n las cadenas
	var numargs=RInStr.arguments.length;
	
	if(numargs<3)
		return n.lastIndexOf(s1)+1;
	else
		return s1.lastIndexOf(s2, n)+1;
}


function ObtSerialNumbersOrPedimento(arrseries, p_numparte)
{
	var xseries = "";
	nlapiLogExecution('DEBUG', 'arrseries.length:   ', arrseries.length);
	for(var j = 0; j < arrseries.length; j++)
	{	
		if(arrseries[j] != null && arrseries[j] != "")
		{
			var fin       = arrseries[j].length;
			var xindex    = arrseries[j].indexOf(")");
			var xnumparte = arrseries[j].substring(0,xindex);	
			xnumparte	  = xnumparte.replace(/\s/g, "");
			
			nlapiLogExecution('DEBUG', 'xnumparte.length:   ', xnumparte.length);
			nlapiLogExecution('DEBUG', 'xnumparte:   ', xnumparte);
			nlapiLogExecution('DEBUG', 'p_numparte:   ', p_numparte);
			
			if(p_numparte == xnumparte)
			{
				xseries   = arrseries[j].substring(xindex + 1,fin);
				xseries   = xseries.replace(/\S\s\S/g, ',');
				xseries   = xseries.replace(/\s/g, "");
				nlapiLogExecution('DEBUG', 'xseries:   ', xseries);
				break;
			}
		}
	}
	return xseries;
}

function ArraySerialNumber(invoice)
{	
	var arrseries = new Array();
	
	for(var i = 1; i <= invoice.getLineItemCount('item'); i++)
	{
		var witem = invoice.getLineItemValue('item', 'item', i);
		if(witem == 23490) //Series
		{
			var series = invoice.getLineItemValue('item','description',i);
			nlapiLogExecution('DEBUG', 'series:   ', series);
			
			if(series != null && series != "")
			{
			   arrseries = series.split('(');			   
			   break;
			} 
		}
	}
	
	return arrseries;
}



function DiscountForItem(line, record)
{
	var amount = 0;
	
	for (var j=1 ; j <= record.getLineItemCount('item'); j++){
		var discline = parseFloat(record.getLineItemValue('item', 'discline', j));
		if (discline == line){
			amount += parseFloat(record.getLineItemValue('item', 'amount', j));
		}
	}
	return amount;
}



function AgroupSerials(xserials)
{
	var numline = 0;
	var strserails = "";
	var separador = ",";
	for(var i = 0; xserials != null && i < xserials.length; i++ )
	{
		numline += 1;
		if(i == 0)
		{  
			strserails = xserials[i]; 
		}
		else
		{
			if(numline == 5)
			{
			  separador = "\n";
			  numline = 0; 
			}
			else
			{
			  separador = ",";
			}
			strserails = strserails + separador + xserials[i];
		}
	
	}

	return strserails;

}




function IEPSItem(line, record)
{
	var amount = 0;
	
	for (var j=1 ; j <= record.getLineItemCount('item'); j++)
	{
		var itemid = parseFloat(record.getLineItemValue('item', 'item', j));
		if (itemid == '1004')
		{
			amount = parseFloat(record.getLineItemValue('item', 'amount', j));
			break;
		}
	}
	
	return amount;

}
